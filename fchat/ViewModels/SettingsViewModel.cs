﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Media;
using Newtonsoft.Json;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.Core;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.Log;
using SapphireChat.Properties;
using Xceed.Wpf.Toolkit;

namespace SapphireChat.ViewModels
{
    public class SettingsViewModel : SuperScreen
    {
        #region Core

        private List<UserAccount> _savedUsers = Settings.Default.UserAccounts.ToList();

        public List<UserAccount> SavedUsers
        {
            get { return _savedUsers; }
            set
            {
                _savedUsers = value;
                NotifyOfPropertyChange(() => SavedUsers);
            }
        }

        public string ChatLogPath
        {
            get { return Settings.Default.ChatLogPath; }
            set
            {
                Settings.Default.ChatLogPath = value;
                SaveSettings();
                NotifyOfPropertyChange(() => ChatLogPath);
            }
        }

        public bool UseSSL
        {
            get { return Settings.Default.UseSSL; }
            set
            {
                Settings.Default.UseSSL = value;
                SaveSettings();
                NotifyOfPropertyChange(() => UseSSL);
            }
        }

        #region Advanced

        private bool _advanced;

        public bool Advanced
        {
            get { return _advanced; }
            set
            {
                if (value)
                {
                    if (
                        MessageBox.Show(
                            "The settings here should not need to be changed unless you know what you are doing.\nContinue?",
                            "Super Serial Warning", System.Windows.MessageBoxButton.YesNo) ==
                        System.Windows.MessageBoxResult.Yes)
                    {
                        _advanced = value;
                        NotifyOfPropertyChange(() => Advanced);
                    }
                }
                else
                {
                    _advanced = value;
                    NotifyOfPropertyChange(() => Advanced);
                }
            }
        }

        public string FChatAddress
        {
            get { return Settings.Default.FCHATADDRESS; }
            set
            {
                Settings.Default.FCHATADDRESS = value;
                SaveSettings();
                NotifyOfPropertyChange(() => FChatAddress);
            }
        }

        public int SSLPort
        {
            get { return Settings.Default.SSLPORT; }
            set
            {
                Settings.Default.SSLPORT = value;
                SaveSettings();
                NotifyOfPropertyChange(() => SSLPort);
            }
        }

        public int SSLTestPort
        {
            get { return Settings.Default.SSLTESTPORT; }
            set
            {
                Settings.Default.SSLTESTPORT = value;
                SaveSettings();
                NotifyOfPropertyChange(() => SSLTestPort);
            }
        }

        public int Port
        {
            get { return Settings.Default.PORT; }
            set
            {
                Settings.Default.PORT = value;
                SaveSettings();
                NotifyOfPropertyChange(() => Port);
            }
        }

        public int TestPort
        {
            get { return Settings.Default.TESTPORT; }
            set
            {
                Settings.Default.TESTPORT = value;
                SaveSettings();
                NotifyOfPropertyChange(() => TestPort);
            }
        }

        #endregion

        public void DeleteUser(UserAccount account)
        {
            if (
                MessageBox.Show("Are you sure you want to remove the saved password for {0}?".F(account.Account),
                                "Art thou sure?", System.Windows.MessageBoxButton.YesNo) ==
                System.Windows.MessageBoxResult.Yes)
            {
                Settings.Default.UserAccounts.Remove(account);
                SaveSettings();
            }
        }

        public void ChatLogPathBrowse()
        {
            var dg = new System.Windows.Forms.FolderBrowserDialog();
            dg.Description = "Select a folder";
            dg.ShowNewFolderButton = true;
            if (dg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                ChatLogPath = dg.SelectedPath;
        }

        #endregion

        #region Colors

        public ObservableCollection<ColorItem> StandardColors
        {
            get
            {
                return new ObservableCollection<ColorItem>
                       {
                           new ColorItem(Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF),
                                         "Default: Chat Foreground/Control Background"),
                           new ColorItem(Color.FromArgb(0xFF, 0x6A, 0x5A, 0xCD),
                                         "Default: Chat Background/Window Color 2"),
                           new ColorItem(Color.FromArgb(0xFF, 0x48, 0x3D, 0x8B), "Default: Window Color 1 & 3"),
                           new ColorItem(Color.FromArgb(0xFF, 0x32, 0xCD, 0x32), "Default: Friend"),
                           new ColorItem(Color.FromArgb(0xFF, 0xFF, 0xFF, 0xBB), "Default: Gender None"),
                           new ColorItem(Color.FromArgb(0xFF, 0x66, 0x99, 0xFF), "Default: Gender Male"),
                           new ColorItem(Color.FromArgb(0xFF, 0xFF, 0x66, 0x99), "Default: Gender Female"),
                           new ColorItem(Color.FromArgb(0xFF, 0xFF, 0x6E, 0xC7), "Default: Gender Transgender"),
                           new ColorItem(Color.FromArgb(0xFF, 0x9B, 0x30, 0xFF), "Default: Gender Hermaphrodite"),
                           new ColorItem(Color.FromArgb(0xFF, 0x00, 0xCC, 0x66), "Default: Gender Male Hermaphrodite"),
                           new ColorItem(Color.FromArgb(0xFF, 0xCC, 0x66, 0xFF), "Default: Gender Cunt Boy"),
                       };
            }
        }

        public Color ChatForeground
        {
            get { return Settings.Default.ChatForeground; }
            set
            {
                Settings.Default.ChatForeground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => ChatForeground);
            }
        }

        public Color ChatBackground
        {
            get { return Settings.Default.ChatBackground; }
            set
            {
                Settings.Default.ChatBackground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => ChatBackground);
            }
        }

        public Color WindowColorOne
        {
            get { return Settings.Default.WindowColorOne; }
            set
            {
                Settings.Default.WindowColorOne = value;
                SaveSettings();
                NotifyOfPropertyChange(() => WindowColorOne);
            }
        }

        public Color WindowColorTwo
        {
            get { return Settings.Default.WindowColorTwo; }
            set
            {
                Settings.Default.WindowColorTwo = value;
                SaveSettings();
                NotifyOfPropertyChange(() => WindowColorTwo);
            }
        }

        public Color WindowColorThree
        {
            get { return Settings.Default.WindowColorThree; }
            set
            {
                Settings.Default.WindowColorThree = value;
                SaveSettings();
                NotifyOfPropertyChange(() => WindowColorThree);
            }
        }

        public Color TextForeground
        {
            get { return Settings.Default.TextForeground; }
            set
            {
                Settings.Default.TextForeground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => TextForeground);
            }
        }

        public Color ButtonMain
        {
            get { return Settings.Default.ButtonMainColor; }
            set
            {
                Settings.Default.ButtonMainColor = value;
                Settings.Default.ButtonMain = value.ToByteArray();
                SaveSettings();
                NotifyOfPropertyChange(() => ButtonMain);
            }
        }

        public Color ButtonFade
        {
            get { return Settings.Default.ButtonFadeColor; }
            set
            {
                Settings.Default.ButtonFadeColor = value;
                Settings.Default.ButtonFade = value.ToByteArray();
                SaveSettings();
                NotifyOfPropertyChange(() => ButtonFade);
            }
        }


        public Color Friend
        {
            get { return Settings.Default.FriendColor; }
            set
            {
                Settings.Default.FriendColor = value;
                SaveSettings();
                NotifyOfPropertyChange(() => Friend);
            }
        }

        public Color GenderNone
        {
            get { return Settings.Default.GenderNone; }
            set
            {
                Settings.Default.GenderNone = value;
                SaveSettings();
                NotifyOfPropertyChange(() => GenderNone);
            }
        }

        public Color GenderMale
        {
            get { return Settings.Default.GenderMale; }
            set
            {
                Settings.Default.GenderMale = value;
                SaveSettings();
                NotifyOfPropertyChange(() => GenderMale);
            }
        }

        public Color GenderFemale
        {
            get { return Settings.Default.GenderFemale; }
            set
            {
                Settings.Default.GenderFemale = value;
                SaveSettings();
                NotifyOfPropertyChange(() => GenderFemale);
            }
        }

        public Color GenderTrans
        {
            get { return Settings.Default.GenderTrans; }
            set
            {
                Settings.Default.GenderTrans = value;
                SaveSettings();
                NotifyOfPropertyChange(() => GenderTrans);
            }
        }

        public Color GenderHerm
        {
            get { return Settings.Default.GenderHerm; }
            set
            {
                Settings.Default.GenderHerm = value;
                SaveSettings();
                NotifyOfPropertyChange(() => GenderHerm);
            }
        }

        public Color GenderMaleHerm
        {
            get { return Settings.Default.GenderMaleHerm; }
            set
            {
                Settings.Default.GenderMaleHerm = value;
                SaveSettings();
                NotifyOfPropertyChange(() => GenderMaleHerm);
            }
        }

        public Color GenderCuntBoy
        {
            get { return Settings.Default.GenderCuntBoy; }
            set
            {
                Settings.Default.GenderCuntBoy = value;
                SaveSettings();
                NotifyOfPropertyChange(() => GenderCuntBoy);
            }
        }

        public Color GenderShemale
        {
            get { return Settings.Default.GenderShemale; }
            set
            {
                Settings.Default.GenderShemale = value;
                SaveSettings();
                NotifyOfPropertyChange(() => GenderShemale);
            }
        }

        public Color ControlForeground
        {
            get { return Settings.Default.ControlForeground; }
            set
            {
                Settings.Default.ControlForeground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => ControlForeground);
            }
        }

        public Color ControlBackground
        {
            get { return Settings.Default.ControlBackground; }
            set
            {
                Settings.Default.ControlBackground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => ControlBackground);
            }
        }

        public Color HighlightForeground
        {
            get { return Settings.Default.HighlightForeground; }
            set
            {
                Settings.Default.HighlightForeground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => HighlightForeground);
            }
        }

        public Color HighlightBackground
        {
            get { return Settings.Default.HighlightBackground; }
            set
            {
                Settings.Default.HighlightBackground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => HighlightBackground);
            }
        }

        public Color OwnTextForeground
        {
            get { return Settings.Default.OwnTextForeground; }
            set
            {
                Settings.Default.OwnTextForeground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => OwnTextForeground);
            }
        }

        public Color OwnTextBackground
        {
            get { return Settings.Default.OwnTextBackground; }
            set
            {
                Settings.Default.OwnTextBackground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => OwnTextBackground);
            }
        }

        public Color LRPBackground
        {
            get { return Settings.Default.LRPBackground; }
            set
            {
                Settings.Default.LRPBackground = value;
                SaveSettings();
                NotifyOfPropertyChange(() => LRPBackground);
            }
        }

        public void ResetChatForeground()
        {
            ChatForeground = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
        }

        public void ResetChatBackground()
        {
            ChatBackground = Color.FromArgb(0xFF, 0x6A, 0x5A, 0xCD);
        }

        public void ResetWindowColorOne()
        {
            WindowColorOne = Color.FromArgb(0xFF, 0x48, 0x3D, 0x8B);
        }

        public void ResetWindowColorTwo()
        {
            WindowColorTwo = Color.FromArgb(0xFF, 0x6A, 0x5A, 0xCD);
        }

        public void ResetWindowColorThree()
        {
            WindowColorThree = Color.FromArgb(0xFF, 0x48, 0x3D, 0x8B);
        }

        public void ResetTextForeground()
        {
            TextForeground = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
        }

        public void ResetButtonMain()
        {
            ButtonMain = Color.FromArgb(0xFF, 0x6A, 0x5A, 0xCD);
        }

        public void ResetButtonFade()
        {
            ButtonFade = Color.FromArgb(0xFF, 0x6A, 0x5A, 0xCD);
        }

        public void ResetFriend()
        {
            Friend = Color.FromArgb(0xFF, 0x32, 0xCD, 0x32);
        }

        public void ResetGenderNone()
        {
            GenderNone = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xBB);
        }

        public void ResetGenderMale()
        {
            GenderMale = Color.FromArgb(0xFF, 0x66, 0x99, 0xFF);
        }

        public void ResetGenderFemale()
        {
            GenderFemale = Color.FromArgb(0xFF, 0xFF, 0x66, 0x99);
        }

        public void ResetGenderTrans()
        {
            GenderTrans = Color.FromArgb(0xFF, 0xFF, 0x6E, 0xC7);
        }

        public void ResetGenderHerm()
        {
            GenderHerm = Color.FromArgb(0xFF, 0x9B, 0x30, 0xFF);
        }

        public void ResetGenderMaleHerm()
        {
            GenderMaleHerm = Color.FromArgb(0xFF, 0x00, 0x7F, 0xFF);
        }

        public void ResetGenderCuntBoy()
        {
            GenderCuntBoy = Color.FromArgb(0xFF, 0x00, 0xCC, 0x66);
        }

        public void ResetGenderShemale()
        {
            GenderShemale = Color.FromArgb(0xFF, 0xCC, 0x66, 0xFF);
        }

        public void ResetControlForeground()
        {
            ControlForeground = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
        }

        public void ResetControlBackground()
        {
            ControlBackground = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
        }

        public void ResetHighlightForeground()
        {
            HighlightForeground = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
        }

        public void ResetHighlightBackground()
        {
            HighlightBackground = Color.FromArgb(0xFF, 0x5A, 0x81, 0xCD);
        }

        public void ResetOwnTextForeground()
        {
            OwnTextForeground = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
        }

        public void ResetLRPBackground()
        {
            LRPBackground = Color.FromArgb(0x33, 0x00, 0xFF, 0x00);
        }

        public void ExportToString()
        {
            WindowManager.CreateDialog(
                new TextInputViewModel("Copy over this string")
                {ShowCancelButton = false, TextInput = ExportedColors().ToBase64()},
                new SapphireWindowManager.WindowOptions
                {
                    CanResize = false,
                    Title = "Export Colors",
                    CanPopout = false
                });
        }

        private string ExportedColors()
        {
            return JsonConvert.SerializeObject(new
                                               {
                                                   CF = ChatForeground,
                                                   CB = ChatBackground,
                                                   WCONE = WindowColorOne,
                                                   WCTWO = WindowColorTwo,
                                                   WCTHREE = WindowColorThree,
                                                   TFG = TextForeground,
                                                   BFB = ButtonMain,
                                                   BBG = ButtonFade,
                                                   F = Friend,
                                                   N = GenderNone,
                                                   M = GenderMale,
                                                   FE = GenderFemale,
                                                   T = GenderTrans,
                                                   H = GenderHerm,
                                                   MH = GenderMaleHerm,
                                                   CBOY = GenderCuntBoy,
                                                   S = GenderShemale,
                                                   CBG = ControlBackground,
                                                   CFG = ControlForeground,
                                                   OTFG = OwnTextForeground,
                                                   OTBG = OwnTextBackground
                                               });
        }

        public void ExportColors()
        {
            var sd = new System.Windows.Forms.SaveFileDialog
                     {
                         AddExtension = true,
                         DefaultExt = ".stf",
                         Title = "Select Save Location",
                         Filter = "Sapphire Theme File (*.stf)|*.stf"
                     };
            if (sd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    using (TextWriter writer = new StreamWriter(sd.FileName, false))
                        writer.WriteLine((ExportedColors()));
                }
                catch (Exception ex)
                {
                    DebugLog.LogMessage("Exception occured during export of theme: {0}", ex.Message);
                }
            }
        }

        public void ImportUsingString()
        {
            var ti = new TextInputViewModel("Paste the string here");
            var window = WindowManager.CreateDialog(ti, new SapphireWindowManager.WindowOptions
                                                      {
                                                          CanResize = false,
                                                          Title = "Import Colors",
                                                          CanPopout = false
                                                      });
            window.Closed += (s, e) =>
                             {
                                 if (ti.DialogResult == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(ti.TextInput))
                                     ImportColors(ti.TextInput.FromBase64());
                             };
        }

        public void ImportColors(string importString = "")
        {
            var od = new System.Windows.Forms.OpenFileDialog
                     {
                         Title = "Select Theme File",
                         Filter = "Sapphire Theme File (*.stf)|*.stf"
                     };
            string json = importString ?? "";
            if (json == "" && od.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                using (TextReader reader = new StreamReader(od.FileName))
                    json = reader.ReadToEnd();

            try
            {
                dynamic stf = JsonConvert.DeserializeObject(json);

                ChatForeground = stf.CF;
                ChatBackground = stf.CB;
                WindowColorOne = stf.WCONE;
                WindowColorTwo = stf.WCTWO;
                WindowColorThree = stf.WCTHREE;
                TextForeground = stf.TFG;
                ButtonMain = stf.BFB;
                ButtonFade = stf.BBG;
                Friend = stf.F;
                GenderNone = stf.N;
                GenderMale = stf.M;
                GenderFemale = stf.FE;
                GenderTrans = stf.T;
                GenderHerm = stf.H;
                GenderMaleHerm = stf.MH;
                GenderCuntBoy = stf.CBOY;
                GenderShemale = stf.S;
                ControlBackground = stf.CBG;
                ControlForeground = stf.CFG;
                OwnTextBackground = stf.OTBG;
                OwnTextForeground = stf.OTFG;
            }
            catch (Exception ex)
            {
                DebugLog.LogMessage("Exception occured during theme import: {0}", ex.Message);
            }
        }

        #endregion

        #region Chat Settings

        public bool EnableIcons
        {
            get { return Settings.Default.EnableIcons; }
            set
            {
                Settings.Default.EnableIcons = value;
                SaveSettings();
                NotifyOfPropertyChange(() => EnableIcons);
            }
        }

        public bool IconHover
        {
            get { return Settings.Default.IconHover; }
            set
            {
                Settings.Default.IconHover = value;
                SaveSettings();
                NotifyOfPropertyChange(() => IconHover);
            }
        }

        public int HoverTime
        {
            get { return Settings.Default.IconHoverTime; }
            set
            {
                Settings.Default.IconHoverTime = value;
                SaveSettings();
            }
        }

        public bool InvasiveIcon
        {
            get { return Settings.Default.InvasiveIcon; }
            set
            {
                Settings.Default.InvasiveIcon = value;
                SaveSettings();
            }
        }

        public bool EnableGenderIcons
        {
            get { return Settings.Default.EnableGenderIcons; }
            set
            {
                Settings.Default.EnableGenderIcons = value;
                SaveSettings();
                NotifyOfPropertyChange(() => EnableGenderIcons);
            }
        }

        public int IconGroupChoice
        {
            get { return Settings.Default.IconChoice; }
            set
            {
                Settings.Default.IconChoice = value;
                SaveSettings();
                NotifyOfPropertyChange(() => IconGroupChoice);
            }
        }

        public FontFamily FontChoice
        {
            get { return Settings.Default.FontChoice; }
            set
            {
                Settings.Default.FontChoice = value;
                SaveSettings();
                NotifyOfPropertyChange(() => FontChoice);
            }
        }

        public bool FontSemiBold
        {
            get { return Settings.Default.FontSemiBold; }
            set
            {
                Settings.Default.FontSemiBold = value;
                SaveSettings();
                NotifyOfPropertyChange(() => FontSemiBold);
            }
        }

        public int FontSize
        {
            get { return Settings.Default.FontSize; }
            set
            {
                Settings.Default.FontSize = value;
                SaveSettings();
                NotifyOfPropertyChange(() => FontSize);
            }
        }

        public bool ShowUserIcon
        {
            get { return Settings.Default.ShowUserIcon; }
            set
            {
                Settings.Default.ShowUserIcon = value;
                SaveSettings();
                NotifyOfPropertyChange(() => ShowUserIcon);
            }
        }

        public bool ShowUserIconMultiLogin
        {
            get { return Settings.Default.ShowUserIconMulti; }
            set
            {
                Settings.Default.ShowUserIconMulti = value;
                SaveSettings();
                NotifyOfPropertyChange(() => ShowUserIconMultiLogin);
            }
        }

        #endregion

        #region Debug

        public bool RunningInDebug
        {
            get { return Settings.Default.UseDebug; }
        }

        public bool UseTestServer
        {
            get { return Settings.Default.UseTestServer; }
            set
            {
                Settings.Default.UseTestServer = value;
                SaveSettings();
                NotifyOfPropertyChange(() => UseTestServer);
            }
        }

        public bool UseDebugLog
        {
            get { return Settings.Default.UseDebug; }
            set
            {
                Settings.Default.UseDebug = value;
                SaveSettings();
                NotifyOfPropertyChange(() => UseDebugLog);
            }
        }

        #endregion

        private void SaveSettings()
        {
            Settings.Default.Save();
            Events.Publish(new EventMessages.SettingsChanged());
        }
    }
}