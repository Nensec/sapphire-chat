﻿using System.Deployment.Application;
using System.Reflection;
using SapphireChat.Infrastructure;

namespace SapphireChat.ViewModels
{
    public class AboutViewModel : SuperScreen
    {
        public string Version
        {
            get
            {
                return "v{0}".F(ApplicationDeployment.IsNetworkDeployed
                           ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
                           : Assembly.GetExecutingAssembly().GetName().Version.ToString());
            }
        }
    }
}