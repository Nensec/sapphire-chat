﻿using System;
using System.Windows.Media;
using Caliburn.Micro;
using SapphireChat.Infrastructure.Events;

namespace SapphireChat.ViewModels
{
    public class ConsoleViewModel : ChatWindow, IHandle<IProduceConsoleLogMessage>, IHandle<ErrorMessages.ErrorMessage>
    {
        public ConsoleViewModel(Guid connectionId) : base(connectionId)
        {
        }

        public override Brush Icon
        {
            get { return System.Windows.Application.Current.FindResource("Monitor") as DrawingBrush; }
        }

        public void Handle(ErrorMessages.ErrorMessage message)
        {
            AddWarningLine("[Error: ({0}) - {1}".F(message.Number, message.Message));
        }

        public void Handle(IProduceConsoleLogMessage message)
        {
            AddLine("{0}".F(message.LogMessage), null);
        }

        public override void DefaultMessageHandler(string input)
        {
            AddLine("[b]You cannot chat in the console![/b]", null, new SolidColorBrush(Colors.DarkRed), new SolidColorBrush(Colors.White));
        }
    }
}