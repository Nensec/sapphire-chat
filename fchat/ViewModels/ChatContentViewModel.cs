﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.Controls;
using SapphireChat.Infrastructure.FChat.Users;
using SapphireChat.Properties;

namespace SapphireChat.ViewModels
{
    public class ChatContentViewModel : SuperScreen
    {
        private Brush _bg;
        private List<Inline> _content;
        private Brush _fg;
        private bool _showIcon;

        private DateTime _timeStamp;

        private User _user;

        public ChatContentViewModel(string content, User user = null, Brush background = null,
                                    Brush foreground = null)
        {
            User = user;
            if (User != null)
                ShowIcon = Settings.Default.EnableGenderIcons;
            BackGround = background ?? Application.Current.FindResource("BrushChatBackground") as Brush;
            ForeGround = foreground ?? Application.Current.FindResource("BrushChatForeground") as Brush;
            Content = new List<Inline> { BBCodeParser.Parse(content) };
            TimeStamp = DateTime.Now;
        }

        public List<Inline> Content
        {
            get { return _content; }
            set
            {
                _content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }

        public DateTime TimeStamp
        {
            get { return _timeStamp; }
            set
            {
                _timeStamp = value;
                NotifyOfPropertyChange(() => TimeStamp);
            }
        }

        public User User
        {
            get { return _user; }
            set
            {
                _user = value;
                NotifyOfPropertyChange(() => User);
            }
        }

        public Brush BackGround
        {
            get { return _bg; }
            set
            {
                _bg = value;
                NotifyOfPropertyChange(() => BackGround);
            }
        }

        public Brush ForeGround
        {
            get { return _fg; }
            set
            {
                _fg = value;
                NotifyOfPropertyChange(() => ForeGround);
            }
        }

        public bool ShowIcon
        {
            get { return _showIcon; }
            set
            {
                _showIcon = value;
                NotifyOfPropertyChange(() => ShowIcon);
            }
        }
    }
}