﻿using System.Linq;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat.Channels;
using SapphireChat.ViewModels.Interfaces;
using System;
using SapphireChat.Infrastructure.FChat;

namespace SapphireChat.ViewModels
{
    public class SocialTabViewModel : SuperScreen
    {
        private bool _canClose = true;
        private IContent _content;
        private string _name = "UNDEFINED";
        public bool Save { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public new bool CanClose
        {
            get { return _canClose; }
            set
            {
                _canClose = value;
                NotifyOfPropertyChange(() => CanClose);
            }
        }

        public new void Close()
        {
            if (Content is ChatChannelViewModel)
            {
                var characters =
                    (Content as ChatChannelViewModel).Channel.ConnectionsToChannel.Select(x => ConnectionManager.GetConnection(x).IdentifiedAs.Identity).ToList();
                if (characters.Count == 1)
                {
                    LeaveChannel(characters[0]);
                    Events.Publish(new EventMessages.TabClosed(_name));
                }
                else
                    WindowManager.CreatePopup(
                        new CharacterSelectViewModel(characters, "", LeaveChannel, addAllLoggedIn: true),
                        new SapphireWindowManager.WindowOptions { Title = "Leave Channel {0}".F(_name), Width = 570, Height = 420 });
            }
            else
                Events.Publish(new EventMessages.TabClosed(_name));
        }

        void LeaveChannel(string character)
        {
            if (character == CharacterSelectViewModel.ALLCHARACTERS)
            {
                foreach (var conn in (Content as ChatChannelViewModel).Channel.ConnectionsToChannel.ToList())
                    ChannelManager.LeaveChannel((Content as ChatChannelViewModel).Channel.Name, conn);
                Events.Publish(new EventMessages.TabClosed(_name));
            }
            else
                ChannelManager.LeaveChannel((Content as ChatChannelViewModel).Channel.Name, ConnectionManager.GetConnection(character).ConnectionId);
        }

        public IContent Content
        {
            get { return _content; }
            set
            {
                _content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }
    }
}