﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Caliburn.Micro;
using Ninject;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.FChat.Users;
using System;
using SapphireChat.ViewModels.Interfaces;
using SapphireChat.Infrastructure.FChat.Channels;

namespace SapphireChat.ViewModels
{
    public class SocialViewModel : Conductor<SocialTabViewModel>.Collection.OneActive,
                                   IHandle<EventMessages.IdentifySuccess>,
                                   IHandle<EventMessages.NewPrivateMessage>,
                                   IHandle<EventMessages.SendPrivateMessage>,
                                   IHandle<EventMessages.JoinChannel>,
                                   IHandle<EventMessages.LeaveChannel>,
                                   IHandle<EventMessages.DisconnectedFromServer>,
                                   IHandle<EventMessages.TabClosed>
    {
        private IEventAggregator _events;

        public SocialViewModel(ConsoleViewModel console)
        {
            console.Events.Subscribe(console);
            Items.Add(new SocialTabViewModel { Name = "Console", CanClose = false, Content = console });
        }

        [Inject]
        public IEventAggregator Events
        {
            get { return _events; }
            set
            {
                _events = value;
                _events.Subscribe(this);
            }
        }

        [Inject]
        public ISapphireWindowManager WindowManager { get; set; }

        public List<User> LoggedInCharacters
        {
            get { return ConnectionManager.LoggedInUsers; }
        }

        public void Handle(EventMessages.DisconnectedFromServer message)
        {
            NotifyOfPropertyChange(() => LoggedInCharacters);
            if (!ConnectionManager.HealthyConnections().Any())
            {
                ActiveItem = Items[0];
                Items.RemoveRange(Items.Skip(1).ToList());
            }
            else
            {
                if (ActiveItem.Content.ConnectionId == message.ConnectionId)
                    ActiveItem = Items[0];
                Items.RemoveRange(Items.Skip(1).Where(x => x.Content.ConnectionId == message.ConnectionId
                                                           && (!(x.Content is ChatChannelViewModel)
                                                               || x.Content is ChatChannelViewModel && (x.Content as ChatChannelViewModel).Channel.ConnectionsToChannel.Count == 1)).ToList());
            }
        }

        public void Handle(EventMessages.IdentifySuccess message)
        {
            if (LoggedInCharacters.Count == 1)
            {
                // Logging in activates either console, or last window should there be an unwanted disconnect
                if (ActiveItem == null)
                    ActiveItem = Items[0];
                else
                    Events.Publish(new EventMessages.ContentChanged(ActiveItem.Content));
            }
            ChannelManager.JoinChannel("ADH-6f029287bbad225b6c50", message.ConnectionId);
            NotifyOfPropertyChange(() => LoggedInCharacters);
        }

        public void Handle(EventMessages.NewPrivateMessage message)
        {
            var priv = Items.SingleOrDefault(x => x.Name.ToLower() == message.Sender.Identity.ToLower() && x.Content.ConnectionId == message.ConnectionId);
            if (priv == null)
                Items.Add(new SocialTabViewModel
                          {
                              Name = message.Sender.Identity,
                              Content = new PrivateChatViewModel(message.Sender, message.ConnectionId, message.Message)
                          });
        }

        public void Handle(EventMessages.SendPrivateMessage message)
        {
            var priv = Items.SingleOrDefault(x => x.Name == message.Receiver.Identity);
            if (priv == null)
                Items.Add(new SocialTabViewModel
                          {
                              Name = message.Receiver.Identity,
                              Content =
                                  new PrivateChatViewModel(message.Receiver, message.ConnectionId, message.Message, true)
                          });
        }

        protected override void OnActivate()
        {
            ActiveItem = Items[0];
            base.OnActivate();
        }

        protected override void ChangeActiveItem(SocialTabViewModel newItem, bool closePrevious)
        {
            Events.Publish(new EventMessages.ContentChanged(newItem.Content));
            ConnectionManager.GetConnection(newItem.Content.ConnectionId).ActiveWindow = newItem.Content;
            if (newItem.Content.ConnectionId != Guid.Empty)
                ServerCommands.LastActiveConnectionId = newItem.Content.ConnectionId;
            base.ChangeActiveItem(newItem, closePrevious);
        }

        public void Channels()
        {
            WindowManager.CreatePopup(new ChannelsViewModel(), new SapphireWindowManager.WindowOptions
                                                                                 {
                                                                                     Title = "Channels",
                                                                                     Width = 570,
                                                                                     Height = 420
                                                                                 });
        }

        public void Login()
        {
            WindowManager.CreatePopup(IoC.Get<LoginShellViewModel>(), new SapphireWindowManager.WindowOptions
            {
                Title = "Login",
                Width = 570,
                Height = 420
            });
        }

        public void Logout()
        {
            if (ConnectionManager.HealthyConnections().Count == 1)
                ConnectionManager.GetConnection().Disconnect();
            else
            {
                WindowManager.CreatePopup(new CharacterSelectViewModel(ConnectionManager.HealthyConnections().Select(x => x.IdentifiedAs.Identity).ToList(), "",
                                                                                         x =>
                                                                                         {
                                                                                             if (x == CharacterSelectViewModel.ALLCHARACTERS)
                                                                                                 foreach (var i in ConnectionManager.HealthyConnections())
                                                                                                     i.Disconnect();
                                                                                             else
                                                                                                 ConnectionManager.GetConnection(x).Disconnect();
                                                                                         }, addAllLoggedIn: true),
                                                            new SapphireWindowManager.WindowOptions
                                                                {
                                                                    Title = "Select character to Logout",
                                                                    Width = 570,
                                                                    Height = 420
                                                                });
            }
        }

        public void Contacts()
        {
            WindowManager.CreatePopup(new ChannelsViewModel(), new SapphireWindowManager.WindowOptions
            {
                Title = "Channels",
                Width = 570,
                Height = 420
            });
        }

        public void Ignores()
        {
            WindowManager.CreatePopup(new ChannelsViewModel(), new SapphireWindowManager.WindowOptions
            {
                Title = "Channels",
                Width = 570,
                Height = 420
            });
        }

        public void FindPartners()
        {
            WindowManager.CreatePopup(new ChannelsViewModel(), new SapphireWindowManager.WindowOptions
            {
                Title = "Channels",
                Width = 570,
                Height = 420
            });
        }

        public void Handle(EventMessages.TabClosed message)
        {
            var tab = Items.SingleOrDefault(x => x.Name == message.TabName);
            if(tab != null)
            {
                if(ActiveItem == tab)
                    ActivateItem(Items[0]);
                Items.Remove(tab);
            }
        }

        public void Handle(EventMessages.JoinChannel message)
        {
            if(ConnectionManager.HealthyConnections().Any(x=>x.IdentifiedAs == message.Character))
            {
                var chan = Items.Where(x => x.Content is ChatChannelViewModel)
                    .SingleOrDefault(
                        x => (x.Content as ChatChannelViewModel).Channel.Name.ToLower() == message.Channel.Name.ToLower());
                if (chan == null)
                    Items.Add(new SocialTabViewModel
                                  {
                                      Name = message.Channel.Name,
                                      Content = new ChatChannelViewModel(message.Channel, message.ConnectionId)
                                  });
                else
                    (chan.Content as ChatChannelViewModel).JoinThisChannel(message.ConnectionId);
            }
        }

        public void Handle(EventMessages.LeaveChannel message)
        {
            var chan = Items.Where(x => x.Content is ChatChannelViewModel).SingleOrDefault(x => (x.Content as ChatChannelViewModel).Channel == message.Channel).Content as ChatChannelViewModel;
            if (chan != null)
                chan.LeaveThisChannel(message.ConnectionId);
        }
    }
}