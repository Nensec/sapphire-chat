﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.FChat.Channels;
using Caliburn.Micro;
using SilverFlow.Controls;

namespace SapphireChat.ViewModels
{
    public class ChannelsViewModel : SuperScreen,
        IHandle<EventMessages.PublicChannelsReceived>,
        IHandle<EventMessages.PrivateChannelsReceived>
    {
        public List<Channel> PublicRooms { get; set; }
        public ObservableCollection<Channel> PrivateRooms { get; set; }

        private string _makeRoomText = "";
        public string MakeRoomText
        {
            get { return _makeRoomText; }
            set
            {
                _makeRoomText = value;
                NotifyOfPropertyChange(() => MakeRoomText);
            }
        }

        private List<Channel> _privateRooms = new List<Channel>();

        public ChannelsViewModel()
        {
            Events.Subscribe(this);
            PublicRooms = new List<Channel>();
            PrivateRooms = new ObservableCollection<Channel>();
            RefreshRooms();
        }

        private string _lastFilter = "";
        public void FilterRooms(string filter)
        {
            _lastFilter = filter;
            PrivateRooms = new ObservableCollection<Channel>(_privateRooms.Where(x => x.Name.ToLower().Contains(filter.ToLower()) || x.Title.ToLower().Contains(filter.ToLower())));
            NotifyOfPropertyChange(() => PrivateRooms);
        }

        public void RefreshRooms()
        {
            ConnectionManager.GetConnection().RequestPublicChannels();
            ConnectionManager.GetConnection().RequestPrivateChannels();
        }

        public void MakeRoom()
        {
            var connectionId = Guid.Empty;
            if (ConnectionManager.HealthyConnections().Count == 1)
                connectionId = ConnectionManager.GetConnection().ConnectionId;
            else
            {
                WindowManager.CreatePopup(new CharacterSelectViewModel(ConnectionManager.HealthyConnections().Select(x => x.IdentifiedAs.Identity).ToList(), "",
                                                                                         x =>
                                                                                         {
                                                                                             connectionId = ConnectionManager.GetConnection(x).ConnectionId;
                                                                                         }), new SapphireWindowManager.WindowOptions
                                                                                         {
                                                                                             Title = "Select character",
                                                                                             Width = 570,
                                                                                             Height = 420
                                                                                         });
            }
            ConnectionManager.GetConnection(connectionId).CreateChannel(MakeRoomText);
            MakeRoomText = "";
        }

        public void JoinChannel(Channel channel)
        {
            if (ConnectionManager.HealthyConnections().Count == 1)
                ChannelManager.JoinChannel(channel.Name, ConnectionManager.HealthyConnections()[0].ConnectionId);
            else
            {
                var window = WindowManager.CreatePopup(new CharacterSelectViewModel(ConnectionManager.HealthyConnections().Select(x => x.IdentifiedAs.Identity).ToList(), "",
                    x =>
                    {
                        if (x == CharacterSelectViewModel.ALLCHARACTERS)
                            foreach (var conn in ConnectionManager.HealthyConnections())
                                ChannelManager.JoinChannel(channel.Name, conn.ConnectionId);
                        else
                            ChannelManager.JoinChannel(channel.Name, ConnectionManager.GetConnection(x).ConnectionId);

                    }, addAllLoggedIn: true), new SapphireWindowManager.WindowOptions { Title = "Join channel {0}".F(channel.Title ?? channel.Name), Width = 670, Height = 420 });
                window.TopMost = true;
            }
        }

        public void Handle(EventMessages.PublicChannelsReceived message)
        {
            PublicRooms = new List<Channel>(message.Channels.OrderBy(x => x.Name));
            NotifyOfPropertyChange(() => PublicRooms);
        }

        public void Handle(EventMessages.PrivateChannelsReceived message)
        {
            _privateRooms = new List<Channel>(message.Channels.OrderBy(x => x.Title));
            FilterRooms(_lastFilter);
        }
    }
}