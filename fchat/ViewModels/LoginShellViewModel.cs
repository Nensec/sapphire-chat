﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Caliburn.Micro;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.ViewModels.Interfaces;
using SilverFlow.Controls;

namespace SapphireChat.ViewModels
{
    public class LoginShellViewModel : Conductor<IScreen>.Collection.OneActive,
                                       IContent,
                                       IHandle<EventMessages.ConnectedToServer>,
                                       IHandle<EventMessages.DisconnectedFromServer>,
                                       IHandle<EventMessages.IdentifySuccess>
    {
        private LoginViewModel login;
        private IEventAggregator _events;
        public Guid ConnectionId { get; private set; }
        private string _lastUser = "";

        public LoginShellViewModel(IEventAggregator events)
        {
            SetLoginVM();
            events.Subscribe(this);
            _events = events;
        }

        #region IContent Members

        public Brush Icon
        {
            get { return null; }
        }


        public void AddLine(string message, Infrastructure.FChat.Users.User character,
                            Brush bg = null, Brush fg = null)
        {
            throw new NotImplementedException();
        }

        public void AddWarningLine(string message)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IHandle<ConnectedToServer> Members

        public void Handle(EventMessages.ConnectedToServer message)
        {
            if (message.ConnectionId == ConnectionId)
            {
                _lastUser = ((LoginViewModel) ActiveItem).UserName;
                var cmds = ConnectionManager.GetConnection(message.ConnectionId);
                ActiveItem = new CharacterSelectViewModel(cmds.Ticket.Characters.ToList(), cmds.Ticket.Default_Character,
                                                          x => ConnectionManager.GetConnection(message.ConnectionId).Identify(x),
                                                          x => ConnectionManager.GetConnection(message.ConnectionId).Disconnect());
            }
        }

        #endregion

        #region IHandle<DisconnectedFromServer> Members

        public void Handle(EventMessages.DisconnectedFromServer message)
        {
            if (message.ConnectionId == ConnectionId)
            {
                SetLoginVM();
                Close();
            }
        }

        #endregion

        #region IHandle<IdentifySuccess> Members

        public void Handle(EventMessages.IdentifySuccess message)
        {
            if (message.ConnectionId == ConnectionId)
            {
                Close();
                _events.Unsubscribe(this);
            }
        }

        #endregion

        private void SetLoginVM()
        {
            if (login == null)
            {
                login = IoC.Get<LoginViewModel>();
                ConnectionId = login.ConnectionId;
            }
                login.UserName = _lastUser;
                ActiveItem = login;
        }

        public void Close()
        {
            var view = Views.First().Value as FrameworkElement;
            if (view == null) return;

            while (view != null && !(view is FloatingWindow))
                view = view.Parent as FrameworkElement;

            if (view != null)
                ((FloatingWindow) view).Close();
        }
    }
}