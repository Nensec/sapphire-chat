﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.FChat.Channels;
using SapphireChat.Infrastructure.FChat;
using Caliburn.Micro;
using SapphireChat.Infrastructure.Events;
using System.Windows;
using SapphireChat.Infrastructure.FChat.Users;
using System.Windows.Documents;
using SapphireChat.Infrastructure.Controls;
using SapphireChat.Properties;

namespace SapphireChat.ViewModels
{
    public class ChatChannelViewModel : ChatWindow,
        IHandle<EventMessages.NewChannelMessage>,
        IHandle<EventMessages.NewLookingForRoleplayMessage>,
        IHandle<EventMessages.ChannelDescriptionChanged>
    {
        public Channel Channel { get; private set; }

        public User CurrentUser
        {
            get { return ConnectionManager.GetConnection(ConnectionId).IdentifiedAs; }
        }

        public ChatChannelViewModel(Channel channel, Guid connectionId) : base(connectionId)
        {
            Channel = channel;
            Channel.ConnectionsToChannel.Add(connectionId);
            Channel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Icon")
                    NotifyOfPropertyChange(() => Icon);
            };
        }

        public void JoinThisChannel(Guid connectionId)
        {
            if (!Channel.ConnectionsToChannel.Contains(connectionId))
                Channel.ConnectionsToChannel.Add(connectionId);
        }

        public void LeaveThisChannel(Guid connectionId)
        {
            Channel.ConnectionsToChannel.Remove(Channel.ConnectionsToChannel.First(x => x == connectionId));
            if (ConnectionId == connectionId && Channel.ConnectionsToChannel.Count > 1)
                ChangeUser(Channel.ConnectionsToChannel[0]);
        }

        public void ChangeUser(Guid connectionId)
        {
            ConnectionId = connectionId;
            NotifyOfPropertyChange(() => CurrentUser);
        }

        public override Brush Icon
        {
            get
            {
                return new ImageBrush(Channel.Icon);
            }
        }

        public List<Inline> ChannelDescription
        {
            get
            {
                return new List<Inline> { BBCodeParser.Parse(Channel.Description) };
            }
        }

        public void SelectUser()
        {
            WindowManager.CreatePopup(new CharacterSelectViewModel(Channel.ConnectionsToChannel.Select(x => ConnectionManager.GetConnection(x).IdentifiedAs.Identity).ToList(), "",
                                                                                     x => ChangeUser(ConnectionManager.GetConnection(x).ConnectionId)),
                                                        new SapphireWindowManager.WindowOptions
                                                            {
                                                                Title = "Select character to chat as",
                                                                Width = 570,
                                                                Height = 420,
                                                            });
        }

        public override void DefaultMessageHandler(string input)
        {
            if(!string.IsNullOrWhiteSpace(input))
            {
                ServerCommands.SendChannelMessage(Channel.Name, input);
                base.DefaultMessageHandler(input);
            }
        }

        public void Handle(EventMessages.NewChannelMessage message)
        {
            if (ConnectionId == message.ConnectionId && Channel == message.Channel)
                AddLine(message.Message, message.User);
        }

        public void Handle(EventMessages.NewLookingForRoleplayMessage message)
        {
            if (ConnectionId == message.ConnectionId && Channel == message.Channel)
                AddLine(message.Message, message.User, Application.Current.FindResource("BrushLRPBackground") as Brush);
        }
    
        public void  Handle(EventMessages.ChannelDescriptionChanged message)
        {
 	        if(message.Channel == Channel && message.ConnectionId == ConnectionId)
                NotifyOfPropertyChange(() => ChannelDescription);
        }

        public override bool ShowUserIcon
        {
            get
            {
                return ConnectionManager.HealthyConnections().Count > 1 || Settings.Default.ShowUserIcon;
            }
        }
    }
}