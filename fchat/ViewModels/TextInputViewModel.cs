﻿using SapphireChat.Infrastructure;

namespace SapphireChat.ViewModels
{
    public class TextInputViewModel : SuperScreen
    {
        public TextInputViewModel(string label)
        {
            TextLabel = label;
            ShowCancelButton = true;
        }

        public string TextLabel { get; private set; }
        public string TextInput { get; set; }
        public bool ShowCancelButton { get; set; }

        public System.Windows.Forms.DialogResult DialogResult { get; set; }

        public void Cancel()
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        public void Ok()
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}