﻿using System;
using System.Windows.Media;
using SapphireChat.Infrastructure.FChat.Users;

namespace SapphireChat.ViewModels.Interfaces
{
    public interface IContent
    {
        Guid ConnectionId { get; }
        Brush Icon { get; }
        void AddLine(string message, User character, Brush bg = null, Brush fg = null);
        void AddWarningLine(string message);
    }
}