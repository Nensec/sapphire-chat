﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Caliburn.Micro;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.FChat.Users;
using SapphireChat.Properties;
using SapphireChat.ViewModels.Interfaces;

namespace SapphireChat.ViewModels
{
    public class ChatWindow : SuperScreen, IContent, IHandle<EventMessages.SettingsChanged>, IHandle<EventMessages.IdentifySuccess>, IHandle<EventMessages.DisconnectedFromServer>
    {
        private ObservableCollection<ChatContentViewModel> _chat = new ObservableCollection<ChatContentViewModel>();
        public Guid ConnectionId { get; set; }

        private string _input = "";

        public ChatWindow(Guid connectionId)
        {
            ConnectionId = connectionId;
            IoC.Get<IEventAggregator>().Subscribe(this);
            // Must be able to do this better.. I got an injected property damnit ;/
        }

        public ObservableCollection<ChatContentViewModel> ChatLines
        {
            get { return _chat; }
            set
            {
                _chat = value;
                NotifyOfPropertyChange(() => ChatLines);
            }
        }

        public virtual bool ShowUserIcon
        {
            get
            {
                if (Settings.Default.ShowUserIcon)
                    if (Settings.Default.ShowUserIconMulti)
                        return ConnectionManager.HealthyConnections().Count > 1;
                    else
                        return true;
                return false;
            }
        }

        public string Input
        {
            get { return _input; }
            set
            {
                _input = value;
                NotifyOfPropertyChange(() => Input);
            }
        }

        public ServerCommands ServerCommands
        {
            get { return ConnectionManager.GetConnection(ConnectionId); }
        }

        #region IContent Members

        public void AddLine(string message, User character, Brush bg = null, Brush fg = null)
        {
            ChatLines.Add(new ChatContentViewModel(message, character ?? new User { Identity = "System" },
                                                   bg, fg));
        }

        public void AddWarningLine(string message)
        {
            AddLine("[b]{0}[/b]".F(message), null, new SolidColorBrush(Colors.DarkRed), new SolidColorBrush(Colors.White));
        }

        public virtual Brush Icon
        {
            get { return null; }
        }

        #endregion

        public virtual void SendMSG(KeyEventArgs args)
        {
            if (Keyboard.Modifiers != ModifierKeys.Shift &&
                args.Key == (System.Windows.Input.Key.Enter | System.Windows.Input.Key.Return))
                CommandProcessor(Input);
        }

        public virtual void SendMSG()
        {
            CommandProcessor(Input);
        }

        public virtual void CommandProcessor(string input)
        {
            if (Input.StartsWith("/"))
                ChatProcessor.ProcessCommand(input, ConnectionId);
            else
                DefaultMessageHandler(Input);
            Input = "";
        }

        public virtual void DefaultMessageHandler(string input)
        {
            AddLine(input, ServerCommands.IdentifiedAs,
                Application.Current.FindResource("BrushOwnTextBackground") as Brush,
                Application.Current.FindResource("BrushOwnTextForeground") as Brush);
        }

        public void Handle(EventMessages.SettingsChanged message)
        {
            NotifyOfPropertyChange(() => ShowUserIcon);
            OnSettingChanged();
        }

        public virtual void OnSettingChanged() { }

        public void Handle(EventMessages.IdentifySuccess message)
        {
            NotifyOfPropertyChange(() => ShowUserIcon);
        }

        public void Handle(EventMessages.DisconnectedFromServer message)
        {
            NotifyOfPropertyChange(() => ShowUserIcon);
        }
    }
}