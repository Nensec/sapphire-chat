﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Properties;

namespace SapphireChat.ViewModels
{
    public class LoginViewModel : SuperScreen, IHandle<EventMessages.LoginFailure>,
                                  IHandle<ErrorMessages.IdentificationFailed>
    {
        private string _error;
        private bool _hasError;
        private bool _isLoginEnabled = true;
        private bool _rememberLogin;
        private IEnumerable<string> _userNames;
        private string _username;

        public LoginViewModel(IEventAggregator events, ServerCommands cmds)
        {
            events.Subscribe(this);
            ConnectionId = cmds.ConnectionId;
            if (Settings.Default.UserAccounts == null)
                Settings.Default.UserAccounts = new Infrastructure.Core.UserAccounts();
        }

        public Guid ConnectionId { get; private set; }

        public string UserName
        {
            get { return _username; }
            set
            {
                _username = value;
                NotifyOfPropertyChange(() => UserName);
            }
        }

        public bool IsLoginEnabled
        {
            get { return _isLoginEnabled; }
            set
            {
                _isLoginEnabled = value;
                NotifyOfPropertyChange(() => IsLoginEnabled);
            }
        }

        public bool HasError
        {
            get { return _hasError; }
            set
            {
                _hasError = value;
                NotifyOfPropertyChange(() => HasError);
            }
        }

        public string ErrorMessage
        {
            get { return _error; }
            set
            {
                _error = value;
                NotifyOfPropertyChange(() => ErrorMessage);
            }
        }

        public IEnumerable<string> UserNames
        {
            get { return Settings.Default.UserAccounts.Select(x => x.Account); }
            set
            {
                _userNames = value;
                NotifyOfPropertyChange(() => UserNames);
            }
        }

        public bool RememberLogin
        {
            get { return _rememberLogin; }
            set
            {
                _rememberLogin = value;
                NotifyOfPropertyChange(() => RememberLogin);
            }
        }

        #region IHandle<IdentificationFailed> Members

        public void Handle(ErrorMessages.IdentificationFailed message)
        {
            HasError = true;
            ErrorMessage = "({0}) - {1}".F(message.Number, message.Message);
            IsLoginEnabled = true;
        }

        #endregion

        #region IHandle<LoginFailure> Members

        public void Handle(EventMessages.LoginFailure message)
        {
            HasError = true;
            ErrorMessage = message.Reason;
            IsLoginEnabled = true;
        }

        #endregion

        public void SetPassword(System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var account = Settings.Default.UserAccounts.GetAccount(e.AddedItems[0] as string);
            if (account != null)
            {
                (Views.First().Value as SapphireChat.Views.LoginView).Password.Password = account.Password;
                RememberLogin = true;
            }
        }

        public void SetPassword(string password)
        {
            (Views.First().Value as SapphireChat.Views.LoginView).Password.Password = password;
        }

        public void Login()
        {
            IsLoginEnabled = false;
            HasError = false;
            if (RememberLogin)
            {
                Settings.Default.UserAccounts.AddOrUpdate(UserName,
                                                          (Views.First().Value as SapphireChat.Views.LoginView).Password
                                                              .Password);
                Settings.Default.Save();
            }
            System.Threading.ThreadPool.QueueUserWorkItem(
                x =>
                ConnectionManager.GetConnection(ConnectionId).Connect(UserName,
                                                                      (Views.First().Value as
                                                                       SapphireChat.Views.LoginView).Password.Password));
        }
    }
}