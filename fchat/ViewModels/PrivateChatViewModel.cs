﻿using System;
using System.Windows;
using System.Windows.Media;
using Caliburn.Micro;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.FChat.Users;
using System.Timers;

namespace SapphireChat.ViewModels
{
    public class PrivateChatViewModel : ChatWindow, IHandle<EventMessages.NewPrivateMessage>
    {
        public PrivateChatViewModel(User character, Guid connectionId, string initialMessage = null, bool isSelf = false)
            : base(connectionId)
        {
            User = character;
            if (initialMessage != null)
                AddLine(initialMessage, isSelf ? ServerCommands.IdentifiedAs : character,
                        isSelf ? Application.Current.FindResource("BrushOwnTextBackground") as Brush : null,
                        isSelf ? Application.Current.FindResource("BrushOwnTextForeground") as Brush : null);
            timer.Elapsed += timer_Elapsed;
        }

        public User CurrentUser
        {
            get { return ConnectionManager.GetConnection(ConnectionId).IdentifiedAs; }
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public User User { get; set; }

        public override Brush Icon
        {
            get
            {
                if (User.Icon == null)
                    User.PropertyChanged += (s, e) =>
                                            {
                                                if (e.PropertyName == "Icon")
                                                    NotifyOfPropertyChange(() => Icon);
                                            };
                return new ImageBrush(User.Icon);
            }
        }

        Timer timer = new Timer(5000) { AutoReset = true };

        static void SendTypingNotification()
        {


        }

        public void Typing(System.Windows.Input.KeyEventArgs args)
        {
            SendMSG(args);
        }

        public void Handle(EventMessages.NewPrivateMessage message)
        {
            if (message.Sender == User)
                AddLine(message.Message, User);
        }

        public override void DefaultMessageHandler(string input)
        {
            ServerCommands.SendPrivateMessage(User.Identity, input);
            base.DefaultMessageHandler(input);
        }
    }
}