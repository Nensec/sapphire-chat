﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.FChat.Users;

namespace SapphireChat.ViewModels
{
    public class CharacterSelectViewModel : SuperScreen
    {
        private BindableCollection<User> _characters = new BindableCollection<User>();

        private User _selectedCharacter;
        private readonly Action<string> _okAction;
        private readonly Action<string> _cancelAction;
        public const string ALLCHARACTERS = "All Available Characters";

        public CharacterSelectViewModel(List<string> characters, string defaultCharacter, Action<string> okAction, Action<string> cancelAction = null, bool addAllLoggedIn = false)
        {
            if (addAllLoggedIn)
                characters.Add(ALLCHARACTERS);
            foreach (var i in characters)
                Characters.Add(new User { Identity = i });
            SelectedCharacter = Characters.SingleOrDefault(x => x.Identity == defaultCharacter);
            _okAction = okAction;
            _cancelAction = cancelAction ?? delegate { };
        }

        public BindableCollection<User> Characters
        {
            get { return _characters; }
            set
            {
                _characters = value;
                NotifyOfPropertyChange(() => Characters);
            }
        }

        public User SelectedCharacter
        {
            get { return _selectedCharacter; }
            set
            {
                _selectedCharacter = value;
                NotifyOfPropertyChange(() => CanOk);
            }
        }

        public bool CanOk
        {
            get { return SelectedCharacter != null; }
        }

        public void Ok()
        {
            _okAction(SelectedCharacter.Identity);
            Close();
        }

        public void Cancel()
        {
            _cancelAction("");
            Close();
        }
    }
}