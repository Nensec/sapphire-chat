﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Caliburn.Micro;
using Ninject;
using SapphireChat.Infrastructure;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.FChat.Users;
using SapphireChat.Infrastructure.Log;
using SapphireChat.ViewModels.Interfaces;
using Action = System.Action;

namespace SapphireChat.ViewModels
{
    internal class ShellViewModel : SuperScreen,
                                    IHandle<EventMessages.ContentChanged>,
                                    IHandle<EventMessages.IdentifySuccess>,
                                    IHandle<EventMessages.DisconnectedFromServer>,
                                    IHandle<EventMessages.LongRunningProcess>,
                                    IHandle<object>
    {
        private string _busyMessage;
        private IContent _content;
        private bool _isBusy;
        private bool _isLoggedIn;
        private int _maxProgress;
        private int _progressValue;
        private List<Type> longRunningTypes = new List<Type>();

        private string versionNr = ApplicationDeployment.IsNetworkDeployed
                                       ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
                                       : Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public ShellViewModel(IEventAggregator events, LoginShellViewModel login)
        {
            events.Subscribe(this);
            DisplayName = "Sapphire Chat by .Nensec v{0}".F(versionNr);
            Content = login;
        }

        [Inject]
        public DebugLog DebugLog { get; set; }

        [Inject]
        public SocialViewModel SocialPanel { get; set; }

        [Inject]
        public ClientCommands ClientCMDs { get; set; }

        [Inject]
        public UserManager Users { get; set; }

        public bool IsLoggedIn
        {
            get { return _isLoggedIn; }
            set
            {
                _isLoggedIn = value;
                NotifyOfPropertyChange(() => IsLoggedIn);
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public string BusyMessage
        {
            get { return _busyMessage; }
            set
            {
                _busyMessage = value;
                NotifyOfPropertyChange(() => BusyMessage);
            }
        }

        public IContent Content
        {
            get { return _content; }
            set
            {
                _content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }

        public int MaxProgressValue
        {
            get { return _maxProgress; }
            set
            {
                _maxProgress = value;
                NotifyOfPropertyChange(() => IsInderterminate);
                NotifyOfPropertyChange(() => MaxProgressValue);
            }
        }

        public int ProgressValue
        {
            get { return _progressValue; }
            set
            {
                _progressValue = value;
                NotifyOfPropertyChange(() => ProgressValue);
            }
        }

        public bool IsInderterminate
        {
            get { return _maxProgress > 0; }
        }

        #region IHandle<ContentChanged> Members

        public void Handle(EventMessages.ContentChanged message)
        {
            Content = message.NewContent;
        }

        #endregion

        #region IHandle<DisconnectedFromServer> Members

        public void Handle(EventMessages.DisconnectedFromServer message)
        {
            if (!ConnectionManager.HealthyConnections().Any())
            {
                Users.Users.CollectionChanged -= UpdateTitleBar;
                DisplayName = "Sapphire Chat by Nensec. v{0}".F(versionNr);
                Content = IoC.Get<LoginShellViewModel>();
                IsLoggedIn = false;
            }
        }

        #endregion

        #region IHandle<IdentifySuccess> Members

        public void Handle(EventMessages.IdentifySuccess message)
        {
            if (ConnectionManager.HealthyConnections().Any())
            {
                UpdateTitleBar(UserManager.Manager.Users, null); // Initial setup, mainly only needed for test~
                Users.Users.CollectionChanged += UpdateTitleBar;
                IsLoggedIn = true;
            }
        }

        #endregion

        #region IHandle<LongRunningProcess> Members

        public void Handle(EventMessages.LongRunningProcess message)
        {
            longRunningTypes.AddRange(message.EndOfProcessEvent);
            IsBusy = true;
            BusyMessage = message.Message;
            MaxProgressValue = message.MaxStep;
            ProgressValue = message.CurrentStep;
        }

        #endregion

        #region IHandle<object> Members

        public void Handle(object message)
        {
            if (longRunningTypes.Contains(message.GetType()))
            {
                longRunningTypes = new List<Type>();
                IsBusy = false;
                MaxProgressValue = 0;
            }
        }

        #endregion

        public void Settings()
        {
            WindowManager.CreatePopup(IoC.Get<SettingsViewModel>(),
                                    new SapphireWindowManager.WindowOptions
                                    {Title = "Settings", Height = 450, Width = 850, Id = "Settings"});
        }

        public void About()
        {
            WindowManager.CreatePopup(new AboutViewModel(), new SapphireWindowManager.WindowOptions
            {
                Title = "About"
            });
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            ThreadPool.QueueUserWorkItem(x =>
                                         {
                                             try
                                             {
                                                 // This looks clunky.. and it kinda is. But this is so far the most reliable way of actually showing the window
                                                 // and making it spawn the messagebox on top. It's the CheckForDetailedUpdate that is the thread blocking action
                                                 // anyway, after that the messagebox should either be shown or not which doesn't block thread.
                                                 var info =
                                                     ApplicationDeployment.CurrentDeployment.CheckForDetailedUpdate();
                                                 Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                                                 {
                                                     if (info.UpdateAvailable &&
                                                         MessageBox.Show(
                                                             "There is a new update available. Download and install it?\r\n\n" +
                                                             "If you cancel, you will be reminded twice on next startup due to the nature of updating system.",
                                                             "New Update", MessageBoxButton.OKCancel,
                                                             MessageBoxImage.Information, MessageBoxResult.OK) == MessageBoxResult.OK)
                                                     {
                                                         ApplicationDeployment.CurrentDeployment.UpdateCompleted +=
                                                             (s, e) =>
                                                             {
                                                                 MessageBox.Show("Update completed. Restarting Sapphire~",
                                                                                 "Update Complete");
                                                                 
                                                                 Application.Current.Exit += (se, ev) => System.Windows.Forms.Application.Restart();
                                                                 Application.Current.Shutdown();
                                                             };
                                                         ApplicationDeployment.CurrentDeployment.UpdateAsync();
                                                     }
                                                 }));
                                             }
                                             catch (InvalidDeploymentException ex)
                                             {
                                                 // Probably in debug mode, in any case auto update won't ever work this way
                                             }
                                             catch (Exception ex)
                                             // Something went wrong with the auto updater. Don't care at this point~
                                             {
                                                 MessageBox.Show(
                                                     "Something went wrong with the auto updater. Please check the f-list forum topic for manual update!",
                                                     "The updater derped!", MessageBoxButton.OK,
                                                     MessageBoxImage.Exclamation);
                                             }
                                         });
        }

        private void UpdateTitleBar(object s, EventArgs e)
        {
            DisplayName = "Sapphire Chat by Nensec. v{0} - Currently online: {1}".F(versionNr, ((IList<User>)s).Count);
        }
    }
}