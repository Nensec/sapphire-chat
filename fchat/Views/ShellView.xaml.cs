﻿using System.Windows;

namespace SapphireChat.Views
{
    public partial class ShellView
    {
        public override void EndInit()
        {
            Style = (Style)FindResource(typeof(Window));
            base.EndInit();
        }
    }
}