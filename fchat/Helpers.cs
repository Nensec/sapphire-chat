﻿using System;
using System.Text;
using System.Windows.Media;

namespace SapphireChat
{
    public static class Helpers
    {
        public static string F(this string s, params object[] args)
        {
            return string.Format(s, args);
        }

        /// <summary>
        /// Data is expected to be ARGB
        /// </summary>
        public static Color ToColor(this byte[] data)
        {
            return Color.FromArgb(data[0], data[1], data[2], data[3]);
        }

        /// <summary>
        /// Converts to ARGB byte array
        /// </summary>
        public static byte[] ToByteArray(this Color color)
        {
            return new[] {color.A, color.R, color.G, color.B};
        }

        public static Color ToMediaColor(this System.Drawing.Color color)
        {
            return new Color
                   {
                       A = color.A,
                       R = color.R,
                       G = color.G,
                       B = color.B,
                   };
        }

        public static System.Drawing.Color ToDrawingColor(this Color color)
        {
            return System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        public static double Clamp(this double value, double min, double max)
        {
            if (value < min)
                value = min;
            if (value > max)
                value = max;
            return value;
        }

        public static string ToBase64(this string str)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
        }

        public static string FromBase64(this string str)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(str));
        }
    }
}