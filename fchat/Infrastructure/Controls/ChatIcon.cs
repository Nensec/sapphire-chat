﻿using System;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using SapphireChat.Infrastructure.FChat.Icons;
using SapphireChat.Properties;

namespace SapphireChat.Infrastructure.Controls
{
    public class ChatIcon : InlineUIContainer, INotifyPropertyChanged
    {
        public Popup PopupImage = new Popup {IsOpen = false, Height = 100, Width = 100};
        private string _character;
        private BitmapImage _icon;

        private Image hoverImage = new Image();

        private DispatcherTimer hoverTimer = new DispatcherTimer
                                             {Interval = TimeSpan.FromMilliseconds(Settings.Default.IconHoverTime)};

        private Image image = new Image {Width = 15, Height = 15};

        public ChatIcon(string character)
        {
            _character = character;
            image.Source = hoverImage.Source = Icon;
            if (Settings.Default.InvasiveIcon)
                image.Width = image.Height = 50;
            Child = image;
            hoverTimer.Tick += hoverTimer_Tick;

            PopupImage.PlacementTarget = Child;
            PopupImage.Placement = PlacementMode.Center;
            PopupImage.PopupAnimation = PopupAnimation.Fade;

            var hlp = new Hyperlink(new InlineUIContainer(hoverImage))
                      {NavigateUri = new Uri("http://www.f-list.net/c/{0}".F(character))};
            hlp.RequestNavigate +=
                (s, e) => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(e.Uri.AbsoluteUri));
            var tbp = new TextBlock(); // fucking ugly solution.. might just work though. Fuckin' ell, it works
            tbp.Inlines.Add(hlp);
            PopupImage.Child = tbp;
        }

        public BitmapImage Icon
        {
            get
            {
                if (_icon == null)
                    return IconManager.GetIcon(_character, x => Icon = x);
                else
                    return new BitmapImage(new Uri("/Images/noimage.bmp", UriKind.Relative));
            }
            set
            {
                _icon = value;
                image.Source = hoverImage.Source = value;
            }
        }

        private bool PopupShowing
        {
            get { return PopupImage.IsOpen; }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        #endregion

        private void Image_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            HidePopup();
        }

        private void hoverTimer_Tick(object sender, EventArgs e)
        {
            hoverTimer.Stop();
            Child.MouseLeave -= Image_MouseLeave;
            PopupImage.IsOpen = true;
            PopupImage.MouseLeave += HoverImage_MouseLeave;
        }

        private void HoverImage_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            HidePopup();
        }

        private void HidePopup()
        {
            PopupImage.IsOpen = false;
        }

        protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            if (!PopupShowing && hoverTimer.IsEnabled)
            {
                hoverTimer.Stop();
                Child.MouseLeave -= Image_MouseLeave;
            }
        }

        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (!PopupShowing && Settings.Default.IconHover)
            {
                hoverTimer.Start();
                Child.MouseLeave += Image_MouseLeave;
            }
        }
    }
}