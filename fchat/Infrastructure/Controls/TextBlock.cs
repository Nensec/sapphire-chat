﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;

namespace SapphireChat.Infrastructure.Controls
{
    public class TextBlock : System.Windows.Controls.TextBlock
    {
        public static readonly DependencyProperty BindableInlinesProperty =
            DependencyProperty.Register("BindableInlines", typeof (List<Inline>), typeof (TextBlock),
                                        new PropertyMetadata(null, (s, e) =>
                                                                   {
                                                                       var tb = s as TextBlock;
                                                                       if (tb == null) return;
                                                                       var nc = e.NewValue as List<Inline>;
                                                                       tb.Inlines.Clear();
                                                                       tb.Inlines.AddRange(nc);
                                                                   }));

        public List<Inline> BindableInlines
        {
            get { return (List<Inline>) GetValue(BindableInlinesProperty); }
            set { SetValue(BindableInlinesProperty, value); }
        }
    }
}