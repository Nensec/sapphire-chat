﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Media;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Properties;
using System.Diagnostics;

namespace SapphireChat.Infrastructure.Controls
{
    class BBTag
    {
        public string Tag { get; set; }
        public Func<string, Brush, Guid, Inline> Text { get; set; }
        public Func<string, object, Brush, Guid, Inline> TextWithData { get; set; }
        public bool CanHasData { get; set; }

        public BBTag(string tag, Func<string, Brush, Guid, Inline> text)
        {
            Tag = tag;
            Text = text;
            CanHasData = false;
        }

        public BBTag(string tag, Func<string, object, Brush, Guid, Inline> text, bool canHasData = false)
        {
            Tag = tag;
            TextWithData = text;
            CanHasData = canHasData;
        }

        public override string ToString() { return Tag; }


        public static List<BBTag> Tags = new List<BBTag>
        {
            new BBTag("noparse", (x, fg, id) => new Run(x) ),
            new BBTag("b", (x, fg, id) => new Text(x, fg, id) { FontWeight = FontWeights.Bold } ),
            new BBTag("s", (x, fg , id) => new Text(x, fg, id) { TextDecorations = TextDecorations.Strikethrough }),
            new BBTag("u", (x, fg , id) => new Text(x, fg, id) { TextDecorations = TextDecorations.Underline }),
            new BBTag("i", (x, fg , id) => new Text(x, fg, id) { FontStyle = FontStyles.Italic }),
            new BBTag("sup", (x, fg, id) => new Text(x, fg, id) { FontSize = 9, BaselineAlignment = BaselineAlignment.TextTop }),
            new BBTag("sub", (x, fg, id) => new Text(x, fg, id) { FontSize = 9, BaselineAlignment = BaselineAlignment.TextBottom }),
            new BBTag("url", (x, fg, id) =>
            {
                if (x.StartsWith("http://") || x.StartsWith("https://") || x.StartsWith("ftp://") || x.StartsWith("ftps://"))
                {
                    var hl = new Hyperlink(new Text(x, fg, id)) { NavigateUri = new Uri(x) };
                    hl.RequestNavigate += (s, e) => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(e.Uri.AbsoluteUri));
                    return hl;
                }
                else
                    return new Run("[Bad format URL: {0}]".F(x));
            }),
            new BBTag("url", (x, d, fg, id) =>
            {
                if ((d as string).StartsWith("http://") || (d as string).StartsWith("https://") || (d as string).StartsWith("ftp://") || (d as string).StartsWith("ftps://"))
                {
                    var hl = new Hyperlink(new Text(x, fg, id)) { NavigateUri = new Uri(d as string) };
                    hl.RequestNavigate += (s,e) => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(e.Uri.AbsoluteUri));
                    return hl;
                }
                else
                    return new Run("[Bad format URL: {0}]".F(d));
            }, true),
            new BBTag("color", (x, d, fg, id) => new Text(x, new SolidColorBrush(GetColor(d as string)), id), true),
            new BBTag("session", (x, d, fg, id) => 
            {
                var hl = new Hyperlink(new Text(x, fg, id));
                hl.RequestNavigate += (s, e) =>  ConnectionManager.GetConnection(id).JoinChannel(d as string);
                return hl;
            }, true),
            new BBTag("channel", (x, d, fg, id) =>
            {
                var hl = new Hyperlink(new Text(x, fg, id)) { NavigateUri = new Uri(x) };
                hl.RequestNavigate += (s, e) => ConnectionManager.GetConnection(id).JoinChannel(x);
                return hl;
            }),
            new BBTag("icon", (x, fg, id) =>
            {
                Hyperlink hl = null;
                if(Settings.Default.EnableIcons)
                    hl = new Hyperlink(new ChatIcon(x)) { NavigateUri = new Uri("http://www.f-list.net/c/{0}".F(x)), TextDecorations = null };
                else
                    hl = new Hyperlink(new Run("[{0}]".F(x)) { Foreground = fg, FontWeight = FontWeights.Bold }) { NavigateUri = new Uri("http://www.f-list.net/c/{0}".F(x)), TextDecorations = null };
                hl.RequestNavigate += (s, e) => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(e.Uri.AbsoluteUri));
                return hl;
            }),
            new BBTag("user", (x, fg, id) =>
            {
                var hl = new Hyperlink(new Run("[{0}]".F(x)) { Foreground = fg, FontWeight = FontWeights.Bold }) { NavigateUri = new Uri("http://www.f-list.net/c/{0}".F(x)), TextDecorations = null };
                hl.RequestNavigate += (s, e) => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(e.Uri.AbsoluteUri));
                return hl;
            })
        };

        public static Color GetColor(string name)
        {
            switch (name)
            {
                case "red": return Colors.Tomato;
                case "blue": return Colors.MediumBlue;
                default:
                case "white": return Colors.White;
                case "pink": return Colors.Salmon;
                case "gray": return Colors.DarkGray;
                case "green": return Colors.LimeGreen;
                case "orange": return Colors.Orange;
                case "purple": return Colors.MediumPurple;
                case "black": return Colors.Black;
                case "brown": return Colors.Chocolate;
                case "cyan": return Colors.DarkTurquoise;
                case "yellow": return Colors.Yellow;
            }
        }

        public static Tuple<Tuple<string, string>, BBTag, string> FindBBTag(string text, BBTag tag, Tuple<Tuple<string, string>, BBTag, string> Match = null)
        {
            var openingTags = Regex.Matches(text, string.Format(@"\[{0}((.|\n)*?)(?:\s*)\]", tag + (tag.CanHasData ? "=" : ""))).Cast<Match>().Where(x => x.Success);
            var closingTags = Regex.Matches(text, string.Format(@"\[/{0}\]", tag)).Cast<Match>().Where(x => x.Success);
            if (openingTags.Any() && closingTags.Any())
            {
                if (Debugger.IsAttached)
                {
                    BBCodeParser.Parse(text);
                }

                List<Tuple<Match, Match, int, string, string>> Tags = new List<Tuple<Match, Match, int, string, string>>();
                foreach (var oTag in openingTags)
                    foreach (var cTag in closingTags)
                    {
                        var length = cTag.Index + cTag.Length - oTag.Index;
                        if (length > 0)
                        {
                            var entireThing = text.Substring(oTag.Index, length);
                            var content = text.Substring(oTag.Index + oTag.Length, length - oTag.Length - cTag.Length);
                            Tags.Add(new Tuple<Match, Match, int, string, string>(oTag, cTag, length, entireThing, content));
                        }
                    }
                var ClosingKeyOpeningValue = new Dictionary<Match, Match>();
                foreach (var i in Tags.OrderBy(x => x.Item3))
                    if (!ClosingKeyOpeningValue.ContainsKey(i.Item2) && !ClosingKeyOpeningValue.ContainsValue(i.Item1))
                        ClosingKeyOpeningValue.Add(i.Item2, i.Item1);
                if (Match == null || ClosingKeyOpeningValue.Max(x => x.Value.Length + x.Key.Length + (x.Key.Index - x.Value.Index)) > Match.Item1.Item1.Length)
                {
                    var biggest = ClosingKeyOpeningValue.OrderByDescending(x => x.Value.Length + x.Key.Length + (x.Key.Index - x.Value.Index)).First();
                    Match = new Tuple<Tuple<string, string>, BBTag, string>(
                            new Tuple<string, string>(text.Substring(biggest.Value.Index, biggest.Key.Index - biggest.Value.Index + biggest.Key.Length),
                                                      text.Substring(biggest.Value.Index + biggest.Value.Length, biggest.Key.Index - biggest.Value.Index - biggest.Value.Length)), tag,
                            biggest.Value.Groups[1].Value);
                }
            }
            return Match;
        }
    }
}