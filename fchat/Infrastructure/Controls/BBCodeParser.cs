﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Text.RegularExpressions;
using SapphireChat.Infrastructure.FChat;

namespace SapphireChat.Infrastructure.Controls
{
    /*
     * Notes:
     * Root span, each section of text is a span, that contains a run, this run has the text, while the span contains the formatting.
     * test [b]bold[/b] ->
     * Span(NormalText) {
     *     Run("test ")
     *   Span(BoldText) {
     *     Run("bold")
     *   }
     * }
     */

    interface IBBCodeTag
    {
        string TagName { get; set; }
        string ParamValue { get; set; }
        bool UseTagList { get; set; }
        bool Whitelist { get; set; }
        HashSet<string> TagList { get; set; }
        bool AllowsTag(string tagName);

    }

    class BBCodeTag : Span, IBBCodeTag
    {
        public BBCodeTag(string name, string param, Color fgColor)
        {
            TagName = name;
            ParamValue = param;
            Foreground = new SolidColorBrush(fgColor);
        }

        public string TagName { get; set; }
        public string ParamValue { get; set; }
        public bool UseTagList { get; set; }
        public bool Whitelist { get; set; }
        public HashSet<string> TagList { get; set; }
        public bool AllowsTag(string tagName)
        {
            if (!UseTagList)
                return true;
            var inList = TagList != null && TagList.Contains(tagName);
            return Whitelist ? inList : !inList;
        }
    }

    class BBCodeTagURL : Hyperlink, IBBCodeTag
    {
        public BBCodeTagURL(string name, string param, Color fgColor)
        {
            TagName = name;
            ParamValue = param;
            Foreground = new SolidColorBrush(fgColor);
        }

        public string TagName { get; set; }
        public string ParamValue { get; set; }
        public bool UseTagList { get; set; }
        public bool Whitelist { get; set; }
        public HashSet<string> TagList { get; set; }
        public bool AllowsTag(string tagName)
        {
            if (!UseTagList)
                return true;
            var inList = TagList != null && TagList.Contains(tagName);
            return Whitelist ? inList : !inList;
        }
    }

    static class BBCodeParser
    {
        enum ParserState
        {
            Text,
            InTag
        }

        private static ParserState _state = ParserState.Text;
        private static int _startPosition;
        private static int _paramStart = -1;
        private static readonly Stack<Span> TagStack = new Stack<Span>();
        private static readonly Stack<Color> ColorStack = new Stack<Color>(); 

        private static readonly Dictionary<string, Func<string, string, Color, Span>> KnownTags =
            new Dictionary<string, Func<string, string, Color, Span>>
                    {
                        {
                            "b",
                            (name, param, color) => new BBCodeTag(name, param, color) { FontWeight = FontWeights.Bold }
                        },
                        {
                            "s",
                            (name, param, color) => new BBCodeTag(name, param, color) { TextDecorations = TextDecorations.Strikethrough }
                        },
                        {
                            "sup",
                            (name, param, color) => new BBCodeTag(name, param, color) { BaselineAlignment = BaselineAlignment.TextTop, 
                                FontSize = 9,
                                UseTagList = true, 
                                TagList = new HashSet<string>(){"sub","sup"} }
                        },
                        {
                            "sub",
                            (name, param, color) => new BBCodeTag(name, param, color) { BaselineAlignment = BaselineAlignment.TextBottom, 
                                FontSize = 9,
                                UseTagList = true, 
                                TagList = new HashSet<string>(){"sub","sup"} }
                        },
                        {
                            "noparse",
                            (name, param, color) => new BBCodeTag(name, param, color) { UseTagList = true, Whitelist = true }
                        },
                        {
                            "color", (name, param, color) =>
                                {
                                    var newColor = GetColor(param);
                                    ColorStack.Push(newColor);
                                    return new BBCodeTag(name, param, newColor);
                                }
                        },
                        {
                            "url",
                            (name, param, color) =>
                                {
                                    var url = new BBCodeTagURL(name, param, color) { UseTagList = true, Whitelist = true };
                                    if (string.IsNullOrWhiteSpace(param)) return url;
                                    if (!param.StartsWith("http://") && !param.StartsWith("https://")
                                        && !param.StartsWith("ftp://"))
                                    {
                                        var badurl = new BBCodeTag(name, param, color) { UseTagList = true, Whitelist = true };
                                        badurl.Inlines.Add(new Run("[Bad format URL: {0}]".F(param)));
                                        return badurl;
                                    }
                                    url.Inlines.Add(new Run());
                                    url.NavigateUri = new Uri(param);
                                    url.RequestNavigate += (s, e) => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(e.Uri.AbsoluteUri));
                                    return url;
                                }
                        },
                        {
                            "channel", (name, param, color) =>
                                {
                                    var url = new BBCodeTagURL(name, param, color) { UseTagList = true, Whitelist = true };
                                    url.Inlines.Add(new Run());
                                    url.NavigateUri = new Uri("http://dummy.dummy/");
                                    url.RequestNavigate += (s, e) => ConnectionManager.GetLastActiveConnection().JoinChannel(param);
                                    return url;
                                }
                        }
                    };

        public static Color GetColor(string name)
        {
            switch (name)
            {
                case "red": return Color.FromRgb(0xfa, 0x2d, 0x2d);
                case "blue": return Color.FromRgb(0x0a, 0x74, 0xff);
                default: return (Application.Current.FindResource("BrushChatForeground") as SolidColorBrush).Color;
                case "white": return Color.FromRgb(0xff, 0xff, 0xff);
                case "pink": return Color.FromRgb(0xfc, 0x74, 0xf6);
                case "gray": return Color.FromRgb(0x5e, 0x5e, 0x5e);
                case "green": return Color.FromRgb(0x00, 0xc9, 0x11);
                case "orange": return Color.FromRgb(0xf2, 0x7c, 0x05);
                case "purple": return Color.FromRgb(0x8f, 0x09, 0xe8);
                case "black": return Color.FromRgb(0x00, 0x00, 0x00);
                case "brown": return Color.FromRgb(0x59, 0x25, 0x00);
                case "cyan": return Color.FromRgb(0x15, 0xc9, 0xed);
                case "yellow": return Color.FromRgb(0xea, 0xff, 0x00);
            }
        }

        private static string PreparseInput(string text)
        {
            text = Regex.Replace(text, @"\[url\]([^\[\]]+)\[/url\]", @"[url=$1]$1[/url]", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, @"\[icon\]([^\[\]]+)\[/icon\]", @"[icon=$1][/icon]", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, @"\[user\]([^\[\]]+)\[/user\]", @"[user=$1]$1[/user]", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, @"\[channel\]([^\[\]]+)\[/channel\]", @"[channel=$1]$1[/channel]", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, @"\[session=([^\[\]]+)\](.+)\[/session\]", @"[session=$2]$1[/session]", RegexOptions.IgnoreCase);
            return text;
        }

        private static void Reset(ref string text, int pos)
        {
            var previousRun = TagStack.Peek().Inlines.Last() as Run;
            if (previousRun != null)
                previousRun.Text += text.Substring(_startPosition, pos - _startPosition + 1);
            _startPosition = pos + 1;
        }

        public static Inline Parse(string text)
        {
            text = PreparseInput(text);
            _startPosition = 0;
            _paramStart = -1;
            ColorStack.Clear();
            ColorStack.Push((Application.Current.FindResource("BrushChatForeground") as SolidColorBrush).Color);
            TagStack.Clear();
            TagStack.Push(new BBCodeTag(null, null, ColorStack.Peek()));
            _state = ParserState.Text;
            for (var i = 0; i < text.Length; ++i)
            {
                var c = text[i];
                switch (_state)
                {
                    case ParserState.Text:
                        {
                            if (c == '[')
                            {
                                TagStack.Peek().Inlines.Add(new Run(text.Substring(_startPosition, i - _startPosition)));
                                _startPosition = i;
                                _state = ParserState.InTag;
                            }
                        }
                        break;
                    case ParserState.InTag:
                        // Tag opening inside a tag. Reset start.
                        switch (c)
                        {
                            case '[':
                                // Smash the new text into the previous Run.
                                var previousRun = TagStack.Peek().Inlines.Last() as Run;
                                if (previousRun != null)
                                    previousRun.Text += text.Substring(_startPosition, i - _startPosition);
                                _startPosition = i;
                                break;
                            case '=':
                                _paramStart = i;
                                break;
                            case ']':
                                var hasParam = _paramStart != -1;
                                var key =
                                    text.Substring(
                                        _startPosition + 1,
                                        hasParam ? _paramStart - _startPosition - 1 : i - _startPosition - 1).ToLower().Trim();
                                var param = hasParam ? text.Substring(_paramStart + 1, i - _paramStart - 1) : string.Empty;
                                _paramStart = -1;
                                var closingTag = key.StartsWith("/");

                                if (closingTag)
                                    key = key.Substring(1).Trim();

                                var isValidTag = KnownTags.ContainsKey(key);
                                // Ensure parent tags allow this tag.
                                if (!isValidTag)
                                {
                                    _state = ParserState.Text;
                                    Reset(ref text, i);
                                    break;
                                }

                                if (closingTag)
                                {
                                    var bbCodeTag = TagStack.Peek() as IBBCodeTag;
                                    if (bbCodeTag != null && bbCodeTag.TagName == key)
                                    {
                                        if (bbCodeTag.TagName == "color" && ColorStack.Count > 1) ColorStack.Pop();
                                        TagStack.Pop();
                                        _state = ParserState.Text;
                                        _startPosition = i + 1;
                                    }
                                    else
                                        Reset(ref text, i);
                                }
                                else
                                {
                                    var allowed = true;
                                    foreach (IBBCodeTag tag in TagStack.OfType<IBBCodeTag>())
                                    {
                                        allowed &= tag.AllowsTag(key);
                                        if (!allowed) break;
                                    }
                                    if (allowed)
                                    {
                                        var currentColor = ColorStack.Peek();
                                        var newTag = KnownTags[key](key, param, currentColor);
                                        TagStack.Peek().Inlines.Add(newTag);
                                        TagStack.Push(newTag);
                                        _startPosition = i + 1;
                                        _state = ParserState.Text;
                                    }
                                    else
                                        Reset(ref text, i + 1);
                                }
                                break;
                        }
                        break;
                }
            }
            if (_startPosition < text.Length)
                TagStack.Peek().Inlines.Add(new Run(text.Substring(_startPosition)));
            return TagStack.Last();
        }
    }
}
