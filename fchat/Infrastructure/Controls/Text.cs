﻿using System;
using System.Windows.Documents;
using System.Windows.Media;

namespace SapphireChat.Infrastructure.Controls
{
    public class Text : Span
    {
        public Inline ParentInline { get; set; }

        public Text(string text, Brush foreground, Guid connectionId)
        {
            Foreground = foreground;
            Tuple<Tuple<string, string>, BBTag, string> Match = null;
            foreach (var tag in BBTag.Tags)
                Match = BBTag.FindBBTag(text, tag, Match) ;
            if (Match == null)
                Inlines.Add(new Run(text));
            else
            {
                var split = text.Split(new[] { Match.Item1.Item1 }, StringSplitOptions.None);
                Inlines.Add(new Text(split[0], foreground, connectionId));
                Inlines.Add(Match.Item2.CanHasData ? Match.Item2.TextWithData(Match.Item1.Item2, Match.Item3, foreground, connectionId) : Match.Item2.Text(Match.Item1.Item2, foreground, connectionId));
                Inlines.Add(new Text(split[1], foreground, connectionId));
            }
        }

        public Text(string text, Brush foreground)
        {
            Foreground = foreground;
            Inlines.Add(new Run(text));
        }

        public void SetParent(Inline parent)
        {
            ParentInline = parent;
            if(Foreground == null)
                Foreground = parent.Foreground;
        }
    }
}