﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Properties;
using SapphireChat.Infrastructure.FChat.Channels;

namespace SapphireChat.Infrastructure.ValueConverters
{
    public class BoolToVisibility : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool visibility = (bool) value;
            return visibility ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
                return DependencyProperty.UnsetValue;
            return null;
        }

        #endregion
    }

    public class MultiplyConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double result = 1.0;
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i] is double)
                    result *= (double) values[i];
            }

            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new Exception("Not implemented");
        }

        #endregion
    }

    public class ByteToColor : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            byte[] result = null;
            if (values[0] is byte[])
                result = (byte[]) ((byte[]) values[0]).Clone();
            else
                return new SolidColorBrush(Colors.Transparent);
            result[0] = (byte) (result[0]*(double) values[1]).Clamp(0, 255);

            return new SolidColorBrush(result.ToColor());
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class GenderToBrushConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return GetGenderColor((Gender) value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        private Brush GetGenderColor(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male:
                    return Application.Current.FindResource("BrushGenderMale") as Brush;
                case Gender.Female:
                    return Application.Current.FindResource("BrushGenderFemale") as Brush;
                case Gender.Transgender:
                    return Application.Current.FindResource("BrushGenderTrans") as Brush;
                case Gender.Herm:
                    return Application.Current.FindResource("BrushGenderHerm") as Brush;
                case Gender.Shemale:
                    return Application.Current.FindResource("BrushGenderShemale") as Brush;
                case Gender.MaleHerm:
                    return Application.Current.FindResource("BrushGenderMaleHerm") as Brush;
                case Gender.CuntBoy:
                    return Application.Current.FindResource("BrushGenderCuntBoy") as Brush;
                case Gender.None:
                default:
                    return Application.Current.FindResource("BrushGenderNone") as Brush;
            }
        }
    }

    public class GenderFillColor : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (Settings.Default.IconChoice == 1)
                return new SolidColorBrush(Colors.Black);
            return new SolidColorBrush(Colors.Transparent);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class GenderToGeometryStrokeConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (Settings.Default.IconChoice)
            {
                case 0:
                default:
                    return GetGenderColor((Gender) value);
                case 1:
                    return new SolidColorBrush(Colors.Gray);
                case 2:
                    return GetGenderColorShaded((Gender) value);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        private object GetGenderColorShaded(Gender gender)
        {
            var femaleColor = (Application.Current.FindResource("BrushGenderFemale") as SolidColorBrush).Color;
            var maleColor = (Application.Current.FindResource("BrushGenderMale") as SolidColorBrush).Color;
            var transColor = (Application.Current.FindResource("BrushGenderTrans") as SolidColorBrush).Color;
            switch (gender)
            {
                case Gender.Male:
                    return new SolidColorBrush(maleColor);
                case Gender.Female:
                    return new SolidColorBrush(femaleColor);
                case Gender.Transgender:
                    return new SolidColorBrush(transColor);
                case Gender.Herm:
                    return new LinearGradientBrush(new GradientStopCollection
                                                   {
                                                       new GradientStop(femaleColor, 0),
                                                       new GradientStop(femaleColor, .3d),
                                                       new GradientStop(Colors.White, .7d),
                                                       new GradientStop(Colors.White, 1)
                                                   }, new Point(0, 0), new Point(1, 0));
                case Gender.Shemale:
                    return new SolidColorBrush(Colors.White);
                case Gender.MaleHerm:
                    return new LinearGradientBrush(new GradientStopCollection
                                                   {
                                                       new GradientStop(maleColor, 0),
                                                       new GradientStop(maleColor, .3d),
                                                       new GradientStop(Colors.White, .7d),
                                                       new GradientStop(Colors.White, 1)
                                                   }, new Point(0, 0), new Point(1, 0));
                case Gender.CuntBoy:
                    return new SolidColorBrush(Colors.White);
                case Gender.None:
                default:
                    return Application.Current.FindResource("BrushGenderNone") as Brush;
            }
        }

        private Brush GetGenderColor(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male:
                    return Application.Current.FindResource("BrushGenderMale") as Brush;
                case Gender.Female:
                    return Application.Current.FindResource("BrushGenderFemale") as Brush;
                case Gender.Transgender:
                    return Application.Current.FindResource("BrushGenderTrans") as Brush;
                case Gender.Herm:
                    return Application.Current.FindResource("BrushGenderHerm") as Brush;
                case Gender.Shemale:
                    return Application.Current.FindResource("BrushGenderShemale") as Brush;
                case Gender.MaleHerm:
                    return Application.Current.FindResource("BrushGenderMaleHerm") as Brush;
                case Gender.CuntBoy:
                    return Application.Current.FindResource("BrushGenderCuntBoy") as Brush;
                case Gender.None:
                default:
                    return Application.Current.FindResource("BrushGenderNone") as Brush;
            }
        }
    }

    public class GenderToGeometryConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (Settings.Default.IconChoice)
            {
                case 0:
                default:
                    return GetGenderDataPackOne((Gender) value);
                case 1:
                    return GetGenderDataPackTwo((Gender) value);
                case 2:
                    return GetGenderDataPackThree((Gender) value);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        private Geometry GetGenderDataPackOne(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male:
                    return Application.Current.FindResource("Male") as Geometry;
                case Gender.Female:
                    return Application.Current.FindResource("Female") as Geometry;
                case Gender.Transgender:
                    return Application.Current.FindResource("Transgender") as Geometry;
                case Gender.Herm:
                    return Application.Current.FindResource("Hermaphrodite") as Geometry;
                case Gender.Shemale:
                    return Application.Current.FindResource("Shemale") as Geometry;
                case Gender.MaleHerm:
                    return Application.Current.FindResource("MaleHermaphrodite") as Geometry;
                case Gender.CuntBoy:
                    return Application.Current.FindResource("Cuntboy") as Geometry;
                case Gender.None:
                default:
                    return Application.Current.FindResource("None") as Geometry;
            }
        }

        private Geometry GetGenderDataPackTwo(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male:
                    return Application.Current.FindResource("Male_Square") as Geometry;
                case Gender.Female:
                    return Application.Current.FindResource("Female_Square") as Geometry;
                case Gender.Transgender:
                    return Application.Current.FindResource("Transgender_Square") as Geometry;
                case Gender.Herm:
                    return Application.Current.FindResource("Herm_Square") as Geometry;
                case Gender.Shemale:
                    return Application.Current.FindResource("Shemale_Square") as Geometry;
                case Gender.MaleHerm:
                    return Application.Current.FindResource("MaleHerm_Square") as Geometry;
                case Gender.CuntBoy:
                    return Application.Current.FindResource("Cuntboy_Square") as Geometry;
                case Gender.None:
                default:
                    return Application.Current.FindResource("None_Square") as Geometry;
            }
        }

        private Geometry GetGenderDataPackThree(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male:
                    return Application.Current.FindResource("Male") as Geometry;
                case Gender.Female:
                    return Application.Current.FindResource("Female") as Geometry;
                case Gender.Transgender:
                    return Application.Current.FindResource("Transgender_Min") as Geometry;
                case Gender.Herm:
                    return Application.Current.FindResource("Female") as Geometry;
                case Gender.Shemale:
                    return Application.Current.FindResource("Female") as Geometry;
                case Gender.MaleHerm:
                    return Application.Current.FindResource("Male") as Geometry;
                case Gender.CuntBoy:
                    return Application.Current.FindResource("Male") as Geometry;
                case Gender.None:
                default:
                    return Application.Current.FindResource("None") as Geometry;
            }
        }
    }

    public class SolidColorBrushToColor : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((SolidColorBrush) value).Color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new SolidColorBrush((Color) value);
        }

        #endregion
    }

    public class BooleanToFontWeight : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return null;
            return (bool)value ? FontWeights.SemiBold : FontWeights.Normal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ChannelToFriendlyName : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Channel))
                return null;
            var c = value as Channel;
            return c.IsPublic ? c.Name : c.Title;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}