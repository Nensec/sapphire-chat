﻿using Caliburn.Micro;
using Ninject.Modules;
using SapphireChat.Infrastructure.Log;

namespace SapphireChat.Infrastructure.LoadModules
{
    internal class MainModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISapphireWindowManager>().To<SapphireWindowManager>().InSingletonScope();
            Bind<IWindowManager>().To<WindowManager>().InSingletonScope();
            Bind<IEventAggregator>().To<EventAggregator>().InSingletonScope();
            Bind<DebugLog>().ToSelf().InSingletonScope();
        }
    }
}