﻿using System;
using Ninject.Modules;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.FChat.Channels;
using SapphireChat.Infrastructure.FChat.Icons;
using SapphireChat.Infrastructure.FChat.Users;
using SapphireChat.ViewModels;

namespace SapphireChat.Infrastructure.LoadModules
{
    public class FChatModule : NinjectModule
    {
        public override void Load()
        {
            Bind<UserManager>().ToSelf().InSingletonScope();
            Bind<ChannelManager>().ToSelf().InSingletonScope();
            Bind<IconManager>().ToSelf().InSingletonScope();
            Bind<ConsoleViewModel>().ToSelf().WithConstructorArgument("connectionId", Guid.Empty);
            Bind<ServerCommands>().ToMethod(x => ConnectionManager.NewConnection());
        }
    }
}