﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace SapphireChat.Infrastructure.Core
{
    [Serializable]
    public class UserAccount
    {
        public string Account { get; set; }

        [XmlElement(ElementName = "Password")]
        public string EncodedPassword { get; set; }

        [XmlIgnore]
        public string Password
        {
            get
            {
                if (EncodedPassword != null)
                    return EncodedPassword.FromBase64();
                return "";
            }
            set { EncodedPassword = value.ToBase64(); }
        }
    }

    [Serializable]
    public class UserAccounts : List<UserAccount>
    {
        public UserAccount GetAccount(string userName)
        {
            return this.SingleOrDefault(x => x.Account == userName);
        }

        public void AddOrUpdate(string username, string password)
        {
            var acc = GetAccount(username);

            if (acc == null)
            {
                this.Add(new UserAccount {Account = username, Password = password});
                return;
            }
            if (acc.Password != password)
                acc.Password = password;
        }
    }
}