﻿using System;

namespace SapphireChat.Infrastructure.Events
{
    public class ErrorMessages
    {
        public static ErrorMessage GenerateEventMessage(ErrorMessage err, Type errorType)
        {
            var error = (ErrorMessage) Activator.CreateInstance(errorType);
            error.Message = err.Message;
            error.Number = err.Number;
            return error;
        }

        #region Nested type: AccountAlreadyBanned

        public class AccountAlreadyBanned : ErrorMessage
        {
        }

        #endregion

        // 9 	"You are banned from the server."

        #region Nested type: AdminRequired

        public class AdminRequired : ErrorMessage
        {
        }

        #endregion

        // 10 	"This command requires that you be an administrator."

        #region Nested type: AlreadyIdentified

        public class AlreadyIdentified : ErrorMessage
        {
        }

        #endregion

        #region Nested type: AlreadyInChannel

        public class AlreadyInChannel : ErrorMessage
        {
        }

        #endregion

        #region Nested type: BannedFromChannel

        public class BannedFromChannel : ErrorMessage
        {
        }

        #endregion

        #region Nested type: BannedFromServer

        public class BannedFromServer : ErrorMessage
        {
        }

        #endregion

        #region Nested type: CannotInvToPubChan

        public class CannotInvToPubChan : ErrorMessage
        {
        }

        #endregion

        // 11 	"Already identified."

        // 20 	"<character name> does not wish to receive messages from you."

        #region Nested type: CannotUseActionOnOp

        public class CannotUseActionOnOp : ErrorMessage
        {
        }

        #endregion

        #region Nested type: ChanDisallowChat

        public class ChanDisallowChat : ErrorMessage
        {
        }

        #endregion

        #region Nested type: ChanDisallowRPAd

        public class ChanDisallowRPAd : ErrorMessage
        {
        }

        #endregion

        #region Nested type: ChanRequiresInvite

        public class ChanRequiresInvite : ErrorMessage
        {
        }

        #endregion

        #region Nested type: ChannelMessageWait

        public class ChannelMessageWait : ErrorMessage
        {
        }

        #endregion

        // 21 	"This action can not be used on a moderator or administrator."

        #region Nested type: ChannelNotLocated

        public class ChannelNotLocated : ErrorMessage
        {
        }

        #endregion

        #region Nested type: ChannelTitleTooLong

        public class ChannelTitleTooLong : ErrorMessage
        {
        }

        #endregion

        // 26 	"Could not locate the requested channel."

        // 40 	"You have been kicked from chat."

        #region Nested type: CharAlreadyChanBanned

        public class CharAlreadyChanBanned : ErrorMessage
        {
        }

        #endregion

        // 41 	"This character is already banned from the channel."

        #region Nested type: CharNotChanBanned

        public class CharNotChanBanned : ErrorMessage
        {
        }

        #endregion

        // 42 	"This character is not currently banned from the channel."

        // 48 	"You are banned from the requested channel."

        #region Nested type: CharNotFoundInChan

        public class CharNotFoundInChan : ErrorMessage
        {
        }

        #endregion

        #region Nested type: CharacterAlreadyChatOp

        public class CharacterAlreadyChatOp : ErrorMessage
        {
        }

        #endregion

        #region Nested type: CharacterIgnoresYou

        public class CharacterIgnoresYou : ErrorMessage
        {
        }

        #endregion

        #region Nested type: CharacterNotChatOp

        public class CharacterNotChatOp : ErrorMessage
        {
        }

        #endregion

        #region Nested type: CharacterNotFound

        public class CharacterNotFound : ErrorMessage
        {
        }

        #endregion

        #region Nested type: CommandError

        public class CommandError : ErrorMessage
        {
        }

        #endregion

        // -2 	"An error occurred while processing your command."

        #region Nested type: CommandNotImplemented

        public class CommandNotImplemented : ErrorMessage
        {
        }

        #endregion

        #region Nested type: CommandRequiresOp

        public class CommandRequiresOp : ErrorMessage
        {
        }

        #endregion

        #region Nested type: DiceRollProblem

        public class DiceRollProblem : ErrorMessage
        {
        }

        #endregion

        #region Nested type: ErrorMessage

        public class ErrorMessage
        {
            public string Message { get; set; }
            public int Number { get; set; }
        }

        #endregion

        #region Nested type: FatalInternalError

        public class FatalInternalError : ErrorMessage
        {
        }

        #endregion

        #region Nested type: IdentificationFailed

        public class IdentificationFailed : ErrorMessage
        {
        }

        #endregion

        #region Nested type: IgnoreListFull

        public class IgnoreListFull : ErrorMessage
        {
        }

        #endregion

        #region Nested type: KickedFromChat

        public class KickedFromChat : ErrorMessage
        {
        }

        #endregion

        #region Nested type: KinkRequestWait

        public class KinkRequestWait : ErrorMessage
        {
        }

        #endregion

        #region Nested type: LoggedInOtherLocation

        public class LoggedInOtherLocation : ErrorMessage
        {
        }

        #endregion

        #region Nested type: LoginTimedOut

        public class LoginTimedOut : ErrorMessage
        {
        }

        #endregion

        #region Nested type: MessageTooLong

        public class MessageTooLong : ErrorMessage
        {
        }

        #endregion

        // 49 	"That character was not found in the channel."

        // 50 	"You must wait five seconds between searches."

        #region Nested type: ModContactWait

        public class ModContactWait : ErrorMessage
        {
        }

        #endregion

        #region Nested type: MustBeInChanToSend

        public class MustBeInChanToSend : ErrorMessage
        {
        }

        #endregion

        #region Nested type: NoDiceFrontpage

        public class NoDiceFrontpage : ErrorMessage
        {
        }

        #endregion

        #region Nested type: NoFreeLoginSlots

        public class NoFreeLoginSlots : ErrorMessage
        {
        }

        #endregion

        // 54 	"Please wait two minutes between calling moderators. If you need to make an addition or a correction to a report, please contact a moderator directly."

        // 61 	"There were too many search terms."

        #region Nested type: NoLoginSlotsFree

        public class NoLoginSlotsFree : ErrorMessage
        {
        }

        #endregion

        #region Nested type: NoSearchResult

        public class NoSearchResult : ErrorMessage
        {
        }

        #endregion

        #region Nested type: OperationSucces

        public class OperationSucces : ErrorMessage
        {
        }

        #endregion

        #region Nested type: ProfileRequestWait

        public class ProfileRequestWait : ErrorMessage
        {
        }

        #endregion

        #region Nested type: RequiresLoggedIn

        public class RequiresLoggedIn : ErrorMessage
        {
        }

        #endregion

        #region Nested type: RolePlayAdWait

        public class RolePlayAdWait : ErrorMessage
        {
        }

        #endregion

        #region Nested type: SearchWait

        public class SearchWait : ErrorMessage
        {
        }

        #endregion

        #region Nested type: SyntaxError

        public class SyntaxError : ErrorMessage
        {
        }

        #endregion

        #region Nested type: TimeoutOutOfRange

        public class TimeoutOutOfRange : ErrorMessage
        {
        }

        #endregion

        // 38 	"The time given for the timeout was invalid. It must be a number between 1 and 90 minutes."

        #region Nested type: TimeoutReceived

        public class TimeoutReceived : ErrorMessage
        {
        }

        #endregion

        #region Nested type: TooManyIPConnections

        public class TooManyIPConnections : ErrorMessage
        {
        }

        #endregion

        // 62 	"There are currently no free login slots."

        // 67 	"Channel titles may not exceed 64 characters in length."

        #region Nested type: TooManySearchResults

        public class TooManySearchResults : ErrorMessage
        {
        }

        #endregion

        #region Nested type: TooManySearchTerms

        public class TooManySearchTerms : ErrorMessage
        {
        }

        #endregion

        #region Nested type: UknownAuthMethod

        public class UknownAuthMethod : ErrorMessage
        {
        }

        #endregion

        #region Nested type: UnknownCommand

        public class UnknownCommand : ErrorMessage
        {
        }

        #endregion

        #region Nested type: UnknownError

        public class UnknownError : ErrorMessage
        {
        }

        #endregion

        // 72 	"There are too many search results, please narrow your search."
    }
}