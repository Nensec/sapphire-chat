﻿using System;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.FChat.Channels;
using SapphireChat.Infrastructure.FChat.Users;
using SapphireChat.ViewModels.Interfaces;
using WebSocket4Net;

namespace SapphireChat.Infrastructure.Events
{
    public class EventMessages
    {
        public class BaseMessage
        {
            public BaseMessage(Guid id)
            {
                ConnectionId = id;
            }

            public Guid ConnectionId { get; private set; }
        }

        public class Broadcast : BaseMessage
        {
            public Broadcast(string message, Guid id)
                : base(id)
            {
                Message = message;
            }

            public string Message { get; private set; }
        }

        public class ChannelDescriptionChanged : BaseMessage
        {
            public ChannelDescriptionChanged(Channel channel, string description, Guid id)
                : base(id)
            {
                Channel = channel;
                Description = description;
            }

            public Channel Channel { get; private set; }
            public string Description { get; private set; }
        }

        public class ConnectedToServer : BaseMessage
        {
            public ConnectedToServer(Guid id) : base(id)
            {
            }
        }

        public class ConnectingToServer : BaseMessage
        {
            public ConnectingToServer(Guid id) : base(id)
            {
            }
        }

        public class ContentChanged
        {
            public ContentChanged(IContent newContent)
            {
                NewContent = newContent;
            }

            public IContent NewContent { get; private set; }
        }

        public class DisconnectedFromServer : BaseMessage
        {
            public DisconnectedFromServer(Guid id, string reason, bool planned) : base(id)
            {
            }

            public bool Planned { get; private set; }
            public string Reason { get; private set; }
        }

        public class IdentifyFail : BaseMessage
        {
            public IdentifyFail(Guid id) : base(id)
            {
            }
        }

        public class IdentifySuccess : BaseMessage, IProduceConsoleLogMessage
        {
            public IdentifySuccess(string character, Guid id)
                : base(id)
            {
                Character = character;
            }

            public string Character { get; private set; }

            public string LogMessage
            {
                get { return "Identification succeeded"; }
            }
        }

        public class LoginFailure : BaseMessage
        {
            public LoginFailure(string reason, Guid id)
                : base(id)
            {
                Reason = reason;
            }

            public string Reason { get; private set; }
        }

        /// <summary>
        /// Needs work now with multi sign in~
        /// </summary>
        public class LongRunningProcess : BaseMessage
        {
            public LongRunningProcess(Type[] endOfProcessEvent, string message, Action cancelAction, Guid id,
                                      int maxStep = 0, int currentstep = 0)
                : base(id)
            {
                EndOfProcessEvent = endOfProcessEvent;
                Message = message;
                MaxStep = maxStep;
                CurrentStep = currentstep;
                CancelAction = cancelAction;
            }

            public Type[] EndOfProcessEvent { get; private set; }
            public string Message { get; private set; }
            public int CurrentStep { get; private set; }
            public int MaxStep { get; private set; }
            public Action CancelAction { get; private set; }
        }

        public class MessageFromServer : BaseMessage
        {
            public MessageFromServer(MessageReceivedEventArgs message, Guid id)
                : base(id)
            {
                Message = message.Message;
            }

            public string Message { get; private set; }
        }

        public class NewPrivateMessage : BaseMessage
        {
            public NewPrivateMessage(User sender, string message, Guid id)
                : base(id)
            {
                Sender = sender;
                Message = message;
            }

            public User Sender { get; private set; }
            public string Message { get; private set; }
        }

        public class NotImplemented : BaseMessage, IProduceConsoleLogMessage
        {
            public NotImplemented(string message, Guid id)
                : base(id)
            {
                Message = "The following method is not implemented (yet!): {0})".F(message);
            }

            public string Message { get; private set; }

            public string LogMessage
            {
                get { return Message; }
            }
        }

        public class SendPrivateMessage : BaseMessage
        {
            public SendPrivateMessage(User receiver, string message, Guid id)
                : base(id)
            {
                Receiver = receiver;
                Message = message;
            }

            public User Receiver { get; private set; }
            public string Message { get; private set; }
        }

        public class ServerSettingChanged
        {
        }

        public class SystemMessage : BaseMessage
        {
            public SystemMessage(string message, string channel, Guid id)
                : base(id)
            {
                Message = message;
                Channel = channel;
            }

            public string Message { get; private set; }
            public string Channel { get; private set; }
        }

        public class TicketReceived : BaseMessage, IProduceConsoleLogMessage
        {
            public TicketReceived(ClientObjects.APITicket ticket, Guid id)
                : base(id)
            {
                Ticket = ticket;
            }

            public ClientObjects.APITicket Ticket { get; private set; }

            public string LogMessage
            {
                get { return "Received ticket: {0}".F(Ticket.Ticket); }
            }
        }

        public class UserConnected : BaseMessage
        {
            public UserConnected(User user, Guid id)
                : base(id)
            {
                User = user;
            }

            public User User { get; private set; }
        }

        public class UserDisconnected : BaseMessage
        {
            public UserDisconnected(string user, Guid id)
                : base(id)
            {
                User = user;
            }

            public string User { get; private set; }
        }

        public class UserJoinedChannel : BaseMessage
        {
            public UserJoinedChannel(Channel channel, User user, Guid id)
                : base(id)
            {
                Channel = channel;
                User = user;
            }

            public Channel Channel { get; private set; }
            public User User { get; private set; }
        }

        public class UserLeftChannel : BaseMessage
        {
            public UserLeftChannel(Channel channel, User user, Guid id)
                : base(id)
            {
                Channel = channel;
                User = user;
            }

            public Channel Channel { get; private set; }
            public User User { get; private set; }
        }

        public class UserStatusChanged : BaseMessage
        {
            public UserStatusChanged(User user, Guid id)
                : base(id)
            {
                User = user;
            }

            public User User { get; private set; }
        }

        public class WebsocketError : BaseMessage
        {
            public WebsocketError(SuperSocket.ClientEngine.ErrorEventArgs error, Guid id)
                : base(id)
            {
                Error = error;
            }

            public SuperSocket.ClientEngine.ErrorEventArgs Error { get; private set; }
        }

        public class TabClosed
        {
            public string TabName { get; private set; }
            public TabClosed(string tabName)
            {
                TabName = tabName;
            }
        }

        public class JoinChannel : BaseMessage
        {
            public Channel Channel { get; private set; }
            public User Character { get; private set; }

            public JoinChannel(Channel channel, User character, Guid id)
                : base(id)
            {
                Channel = channel;
                Character = character;
            }
        }

        public class ReceivedChannelData : BaseMessage
        {
            public Channel Channel { get; private set; }
            public User[] Users { get; private set; }
            public string Title { get; private set; }

            public ReceivedChannelData(Channel channel, string title, User[] users, Guid id)
                : base(id)
            {
                Channel = channel;
                Title = title;
                Users = users;
            }
        }

        public class NewChannelMessage : BaseMessage
        {
            public Channel Channel { get; private set; }
            public User User { get; private set; }
            public string Message { get; private set; }

            public NewChannelMessage(Channel channel, User user, string message, Guid id)
                : base(id)
            {
                Channel = channel;
                User = user;
                Message = message;
            }
        }

        public class NewLookingForRoleplayMessage : BaseMessage
        {
            public Channel Channel { get; private set; }
            public User User { get; private set; }
            public string Message { get; private set; }

            public NewLookingForRoleplayMessage(Channel channel, User user, string message, Guid id)
                : base(id)
            {
                Channel = channel;
                User = user;
                Message = message;
            }
        }

        public class PublicChannelsReceived
        {
            public Channel[] Channels { get; private set; }

            public PublicChannelsReceived(Channel[] channels)
            {
                Channels = channels;
            }
        }

        public class PrivateChannelsReceived
        {
            public Channel[] Channels { get; private set; }

            public PrivateChannelsReceived(Channel[] channels)
            {
                Channels = channels;
            }
        }

        public class LeaveChannel : BaseMessage
        {
            public Channel Channel { get; private set; }
            public User Character { get; private set; }

            public LeaveChannel(Channel channel, User character, Guid id)
                : base(id)
            {
                Channel = channel;
                Character = character;
            }
        }

        public class SettingsChanged
        {
        }
    }

    public interface IProduceConsoleLogMessage
    {
        string LogMessage { get; }
    }
}