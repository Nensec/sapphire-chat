﻿namespace SapphireChat.Infrastructure.Events
{
    public enum MessageType
    {
        Channel,
        Private,
        Broadcast
    }
}