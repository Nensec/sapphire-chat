﻿using System.Linq;
using System.Windows;
using Caliburn.Micro;
using Ninject;
using SilverFlow.Controls;

namespace SapphireChat.Infrastructure
{
    public class SuperScreen : Screen
    {
        private IEventAggregator _events = null;
        [Inject]
        public IEventAggregator Events
        {
            get { return _events ?? (_events = IoC.Get<IEventAggregator>()); }
            set { _events = value; }
        }

        private ISapphireWindowManager _windowManager;
        [Inject]
        public ISapphireWindowManager WindowManager
        {
            get { return _windowManager ?? (_windowManager = IoC.Get<ISapphireWindowManager>()); }
            set { _windowManager = value; }
        }

        public void Close()
        {
            var view = Views.First().Value as FrameworkElement;
            if (view == null) return;

            while (view != null && !(view is FloatingWindow))
                view = view.Parent as FrameworkElement;

            if (view != null)
                ((FloatingWindow) view).Close();
        }
    }
}