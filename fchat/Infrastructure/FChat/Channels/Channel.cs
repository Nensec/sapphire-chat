﻿using System;
using System.Collections.Generic;
using System.Linq;
using SapphireChat.Infrastructure.FChat.Users;
using System.ComponentModel;
using SapphireChat.Infrastructure.Controls;
using System.Windows.Media.Imaging;
using Caliburn.Micro;
using SapphireChat.Infrastructure.Events;

namespace SapphireChat.Infrastructure.FChat.Channels
{
    public class Channel : INotifyPropertyChanged,
        IEqualityComparer<Channel>,
        IHandle<EventMessages.ChannelDescriptionChanged>,
        IHandle<EventMessages.ReceivedChannelData>,
        IHandle<EventMessages.UserJoinedChannel>,
        IHandle<EventMessages.UserLeftChannel>
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public ChannelMode Mode { get; set; }
        public int Characters { get; set; }
        public BindableCollection<User> Users { get; set; }
        public List<Guid> ConnectionsToChannel { get; set; }
        private BitmapImage _icon;
        public BitmapImage Icon
        {
            get { return _icon; }
            set
            {
                _icon = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Icon"));
            }
        }

        private string _description = "";
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                ParseDescriptionForIcon(value);
                PropertyChanged(this, new PropertyChangedEventArgs("Description"));
            }
        }

        public bool IsPublic { get; set; }

        public Channel()
        {
            Title = "";
            Users = new BindableCollection<User>();
            ConnectionsToChannel = new List<Guid>();
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        private void ParseDescriptionForIcon(string desc)
        {
            var match = BBTag.FindBBTag(desc, new BBTag("icon", (x, d, fg, id) =>
                                                                      {
                                                                          if (d as string == "channelIcon")
                                                                          {
                                                                              var user = UserManager.GetUser(x);
                                                                              Icon = user.Icon;
                                                                          }
                                                                          return null;
                                                                      })) ?? BBTag.FindBBTag(desc, new BBTag("icon", (x, fg, id) =>
                                                                                                                         {
                                                                                                                             var user = UserManager.GetUser(x);
                                                                                                                             Icon = user.Icon;
                                                                                                                             return null;
                                                                                                                         }));
            // Resharper WTF moment

            if (match == null) return;
            match.Item2.TextWithData(match.Item1.Item2, match.Item3, null, Guid.Empty);
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public void Handle(EventMessages.ChannelDescriptionChanged message)
        {
            if(message.Channel != this) return;
            Description = message.Description;
        }
        
        public void Handle(EventMessages.ReceivedChannelData message)
        {
            if (message.Channel != this) return;
            Title = message.Title;
            Users = new BindableCollection<User>(Users.Union(message.Users));
        }

        public bool Equals(Channel x, Channel y)
        {
            return x.Name.ToLower() == y.Name.ToLower();
        }

        public int GetHashCode(Channel obj)
        {
            return obj.Description.GetHashCode() + obj.Name.GetHashCode();
        }

        public void Handle(EventMessages.UserJoinedChannel message)
        {
            if (message.Channel != this) return;
            if(!Users.Contains(message.User))
                Users.Add(message.User);
        }

        public void Handle(EventMessages.UserLeftChannel message)
        {
            if (message.Channel != this) return;
            if(Users.Contains(message.User))
                Users.Remove(message.User);
        }
    }
}