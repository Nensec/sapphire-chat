﻿using System.Linq;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat.Users;

namespace SapphireChat.Infrastructure.FChat.Channels
{
    public class ChannelManager : IHandle<EventMessages.DisconnectedFromServer>,
        IHandle<EventMessages.PublicChannelsReceived>,
        IHandle<EventMessages.PrivateChannelsReceived>
    {
        private static ChannelManager _manager;
        private static IEventAggregator _events;

        private static IEventAggregator Events
        {
            get { return _events ?? (_events = IoC.Get<IEventAggregator>()); }
        }
        
        public ChannelManager()
        {
            JoinedChannels = new BindableCollection<Channel>();
            Events.Subscribe(this);
        }

        public static ChannelManager Manager
        {
            get { return _manager ?? (_manager = IoC.Get<ChannelManager>()); }
        }

        public List<Channel> PrivateChannelCache { get; set; }
        public List<Channel> PublicChannelCache { get; set; }
        public BindableCollection<Channel> JoinedChannels { get; set; }

        public static IEnumerable<Channel> GetPrivateChannel(string name)
        {
            return Manager.PrivateChannelCache.Where(x => x.Name == name || x.Title == name);
        }

        public static Channel GetPublicChannel(string name)
        {
            return Manager.PublicChannelCache.SingleOrDefault(x => x.Name == name);
        }

        public static Channel GetJoinedChannel(string channel)
        {
            return Manager.JoinedChannels.SingleOrDefault(x => x.Name.ToLower() == channel.ToLower());
        }

        public static List<Channel> GetJoinedChannelsForConnection(Guid connectionId)
        {
            return Manager.JoinedChannels.Where(x => x.ConnectionsToChannel.Contains(connectionId)).ToList();
        }

        public static Channel JoinChannel(string channel, Guid connectionId, User character = null)
        {
            var chan = Manager.JoinedChannels.All(x => x.Name != channel) ? AddJoinedChannel(channel, ChannelMode.Both, 0) : Manager.JoinedChannels.First(x => x.Name == channel);
            ConnectionManager.GetConnection(connectionId).JoinChannel(channel);
            Events.Publish(new EventMessages.JoinChannel(chan, character ?? ConnectionManager.GetConnection(connectionId).IdentifiedAs, connectionId));
            return chan;
        }

        public static void LeaveChannel(string channel, Guid connectionId, User character = null)
        {
            var chan = GetJoinedChannelsForConnection(connectionId).FirstOrDefault(x => x.Name == channel);
            if (chan != null)
            {
                Manager.JoinedChannels.Remove(chan);
                ConnectionManager.GetConnection(connectionId).LeaveChannel(channel);
                Events.Publish(new EventMessages.LeaveChannel(chan, character ?? ConnectionManager.GetConnection(connectionId).IdentifiedAs, connectionId));
            }
        }

        public static Channel AddJoinedChannel(string channel, ChannelMode mode, int characters, bool isPublic = false)
        {
            var chan = new Channel { Name = channel, Mode = mode, Characters = characters, IsPublic = isPublic };
            if (Manager.JoinedChannels.All(x => x.Name.ToLower() != channel.ToLower()))
                Manager.JoinedChannels.Add(chan);
            return chan;
        }

        public static void RemoveJoinedChannel(string channel)
        {
            var chan = Manager.JoinedChannels.SingleOrDefault(x => x.Name.ToLower() == channel.ToLower());
            if (chan != null)
                Manager.JoinedChannels.Remove(chan);
        }

        public void Handle(EventMessages.DisconnectedFromServer message)
        {
            foreach (var channel in Manager.JoinedChannels.Where(x => x.ConnectionsToChannel.Contains(message.ConnectionId)))
                channel.ConnectionsToChannel.Remove(message.ConnectionId);
        }

        public void Handle(EventMessages.PublicChannelsReceived message)
        {
            PublicChannelCache = new List<Channel>(message.Channels);
        }

        public void Handle(EventMessages.PrivateChannelsReceived message)
        {
            PrivateChannelCache = new List<Channel>(message.Channels);
        }
    }
}