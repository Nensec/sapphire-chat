﻿using Newtonsoft.Json;

namespace SapphireChat.Infrastructure.FChat
{
    /// <summary>
    /// Objects send to the server
    /// </summary>
    public class ServerObjects
    {
        #region Nested type: ACB

        public class ACB : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: AOP

        public class AOP : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: AWC

        public class AWC : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: BRO

        public class BRO : IServerObject
        {
            [JsonProperty("message")]
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: CBL

        public class CBL : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: CBU

        public class CBU : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }

            [JsonProperty("channel")]
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: CCR

        public class CCR : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: CDS

        public class CDS : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("description")]
            public string Description { get; set; }
        }

        #endregion

        #region Nested type: CIU

        public class CIU : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: CKU

        public class CKU : IServerObject
        {
            public string Channel { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: COA

        public class COA : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: COL

        public class COL : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: COR

        public class COR : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: CRC

        public class CRC : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: CSO

        public class CSO : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }

            [JsonProperty("channel")]
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: CTU

        public class CTU : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }

            public int Length { get; set; }
        }

        #endregion

        #region Nested type: CUB

        public class CUB : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: DOP

        public class DOP : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: FKS

        public class FKS : IServerObject
        {
            [JsonProperty("kinks")]
            public int[] Kinks { get; set; }

            [JsonProperty("genders")]
            public Gender[] Genders { get; set; }

            [JsonProperty("orientations")]
            public Orientation[] Orientations { get; set; }

            [JsonProperty("languages")]
            public Language[] Languages { get; set; }

            [JsonProperty("furryprefs")]
            public FurryPref[] FurryPrefs { get; set; }

            [JsonProperty("roles")]
            public Role[] Roles { get; set; }
        }

        #endregion

        #region Nested type: IDN

        public class IDN : IServerObject
        {
            [JsonProperty("method")]
            public string Method
            {
                get { return "ticket"; }
            }

            [JsonProperty("account")]
            public string Account { get; set; }

            [JsonProperty("ticket")]
            public string Ticket { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }

            [JsonProperty("cname")]
            public string CName { get; set; }

            [JsonProperty("cversion")]
            public string CVersion { get; set; }
        }

        #endregion

        #region Nested type: IGN

        public class IGN : IServerObject
        {
            [JsonProperty("action")]
            public IgnoreAction Action { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: JCH

        public class JCH : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: KIK

        public class KIK : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: KIN

        public class KIN : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: LCH

        public class LCH : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: LFR

        public class LFR : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: MSG

        public class MSG : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: PRI

        public class PRI : IServerObject
        {
            [JsonProperty("recipient")]
            public string Recipient { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: PRO

        public class PRO : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: RLD

        public class RLD : IServerObject
        {
            [JsonProperty("save")]
            public string Save { get; set; }
        }

        #endregion

        #region Nested type: RLL

        public class RLL : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("dice")]
            public string Dice { get; set; }
        }

        #endregion

        #region Nested type: RMO

        public class RMO : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("mode")]
            public ChannelMode Mode { get; set; }
        }

        #endregion

        #region Nested type: RST

        public class RST : IServerObject
        {
            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("status")]
            public RoomStatus Status { get; set; }
        }

        #endregion

        #region Nested type: RWD

        public class RWD : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: SFC

        public class SFC : IServerObject
        {
            [JsonProperty("action")]
            public string Action
            {
                get { return "report"; }
            }

            [JsonProperty("report")]
            public string Report { get; set; }

            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: STA

        public class STA : IServerObject
        {
            [JsonProperty("status")]
            public CharacterStatus Status { get; set; }

            [JsonProperty("statusmsg")]
            public string StatusMSG { get; set; }
        }

        #endregion

        #region Nested type: TMO

        public class TMO : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }

            [JsonProperty("time")]
            public int Time { get; set; }

            [JsonProperty("reason")]
            public string Reason { get; set; }
        }

        #endregion

        #region Nested type: TPN

        public class TPN : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }

            [JsonProperty("status")]
            public TypingStatus Status { get; set; }
        }

        #endregion

        #region Nested type: UBN

        public class UBN : IServerObject
        {
            [JsonProperty("character")]
            public string Character { get; set; }
        }

        #endregion
    }

    public interface IServerObject
    {
    }
}