﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Newtonsoft.Json;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat.Channels;
using SapphireChat.Infrastructure.FChat.Users;

namespace SapphireChat.Infrastructure.FChat
{
    /// <summary>
    /// Messages handled by the client, send from the server
    /// </summary>
    public class ClientCommands : IHandle<EventMessages.MessageFromServer>
    {
        // All messages that the server can send to the client~
        private readonly List<ClientMessage> _messages = new List<ClientMessage>();

        public ClientCommands(IEventAggregator events)
        {
            Events = events;
            Events.Subscribe(this);

            _messages.Add(new ClientMessage
                         {
                             Message = "ADL",
                             Config = (id, data) =>
                                      {
                                          var adl = JsonConvert.DeserializeObject<ClientObjects.ADL>(data);
                                          UserManager.AddOps(adl.Ops);
                                      }
                         });
            _messages.Add(new ClientMessage
                         {
                             Message = "AOP",
                             Config = (id, data) =>
                                      {
                                          var aop = JsonConvert.DeserializeObject<ClientObjects.AOP>(data);
                                          UserManager.AddOp(aop.Character);
                                      }
                         });
            _messages.Add(new ClientMessage
                         {
                             Message = "BRO",
                             Config = (id, data) =>
                                      {
                                          var bro = JsonConvert.DeserializeObject<ClientObjects.BRO>(data);
                                          Events.Publish(new EventMessages.Broadcast(bro.Message, id));
                                      }
                         });
            _messages.Add(new ClientMessage
                         {
                             Message = "CDS",
                             Config = (id, data) =>
                                      {
                                          var cds = JsonConvert.DeserializeObject<ClientObjects.CDS>(data);
                                          Events.Publish(new EventMessages.ChannelDescriptionChanged(ChannelManager.GetJoinedChannel(cds.Channel), cds.Description, id));
                                      }
                         });
            _messages.Add(new ClientMessage
                         {
                             Message = "CHA",
                             Config = (id, data) =>
                                          {
                                              var cha = JsonConvert.DeserializeObject<ClientObjects.CHA>(data);
                                              Events.Publish(new EventMessages.PublicChannelsReceived(cha.Channels));
                                          }
                         });
            _messages.Add(new ClientMessage { Message = "CIU" });
            _messages.Add(new ClientMessage { Message = "CBU" });
            _messages.Add(new ClientMessage { Message = "CKU" });
            _messages.Add(new ClientMessage { Message = "COA" });
            _messages.Add(new ClientMessage { Message = "COR" });
            _messages.Add(new ClientMessage { Message = "COL" });
            _messages.Add(new ClientMessage { Message = "CON" });
            _messages.Add(new ClientMessage { Message = "CSO" });
            _messages.Add(new ClientMessage { Message = "DOP" });
            _messages.Add(new ClientMessage
                         {
                             Message = "ERR",
                             Config =
                                 (id, data) =>
                                 HandeErrorMessage(JsonConvert.DeserializeObject<ErrorMessages.ErrorMessage>(data))
                         });
            _messages.Add(new ClientMessage { Message = "FKS" });
            _messages.Add(new ClientMessage
                         {
                             Message = "FLN",
                             Config = (id, data) =>
                                      {
                                          var user = JsonConvert.DeserializeObject<ClientObjects.FLN>(data).Character;
                                          UserManager.RemoveUser(user);
                                          Events.Publish(new EventMessages.UserDisconnected(user, id));
                                      }
                         });
            _messages.Add(new ClientMessage { Message = "HLO" });
            _messages.Add(new ClientMessage
                         {
                             Message = "ICH",
                             Config = (id, data) =>
                                      {
                                          var ich = JsonConvert.DeserializeObject<ClientObjects.ICH>(data);
                                          Events.Publish(
                                              new EventMessages.ReceivedChannelData(
                                                  ChannelManager.GetJoinedChannel(ich.Channel), ich.Title, ich.Users, id));
                                      }
                         });
            _messages.Add(new ClientMessage
                         {
                             Message = "IDN",
                             Config = (id, data) =>
                                      {
                                          var idn = JsonConvert.DeserializeObject<ClientObjects.IDN>(data);
                                          ConnectionManager.GetConnection(id).IdentifiedAs =
                                              UserManager.GetUser(idn.Character);
                                          ServerCommands.LastActiveConnectionId = id;
                                          Events.Publish(new EventMessages.IdentifySuccess(idn.Character, id));
                                      }
                         });
            _messages.Add(new ClientMessage
                         {
                             Message = "JCH",
                             Config = (id, data) =>
                                      {
                                          var jch = JsonConvert.DeserializeObject<ClientObjects.JCH>(data);
                                          Events.Publish(new EventMessages.UserJoinedChannel(ChannelManager.GetJoinedChannel(jch.Channel), jch.Character, id));
                                      }
                         });
            _messages.Add(new ClientMessage { Message = "KID" });
            _messages.Add(new ClientMessage
            {
                Message = "LCH",
                Config = (id, data) =>
                             {
                                 var lch = JsonConvert.DeserializeObject<ClientObjects.LCH>(data);
                                 Events.Publish(new EventMessages.UserLeftChannel(ChannelManager.GetJoinedChannel(lch.Channel), UserManager.GetUser(lch.Character), id));
                             }
            });
            _messages.Add(new ClientMessage
                         {
                             Message = "LIS",
                             Config = (id, data) =>
                                      {
                                          var lis = JsonConvert.DeserializeObject<ClientObjects.LIS>(data);
                                          UserManager.AddRange(lis.Characters);
                                      }
                         });
            _messages.Add(new ClientMessage
                             {
                                 Message = "LRP",
                                 Config = (id, data) =>
                                              {
                                                  var lrp = JsonConvert.DeserializeObject<ClientObjects.LRP>(data);
                                                  Events.Publish(new EventMessages.NewLookingForRoleplayMessage(ChannelManager.GetJoinedChannel(lrp.Channel),
                                                      UserManager.GetUser(lrp.Character), lrp.Message, id));
                                              }
                             });
            _messages.Add(new ClientMessage
            {
                Message = "MSG",
                Config =
                    (id, data) =>
                    {
                        var msg = JsonConvert.DeserializeObject<ClientObjects.MSG>(data);
                        Events.Publish(new EventMessages.NewChannelMessage(ChannelManager.GetJoinedChannel(msg.Channel),
                                                                           UserManager.GetUser(msg.Character), msg.Message, id));
                    }
            });
            _messages.Add(new ClientMessage
                         {
                             Message = "NLN",
                             Config = (id, data) =>
                                      {
                                          var nln = JsonConvert.DeserializeObject<ClientObjects.NLN>(data);
                                          var user = new User { Identity = nln.Identity, Status = nln.Status, Gender = nln.Gender };
                                          UserManager.AddUser(user);
                                          Events.Publish(new EventMessages.UserConnected(user, id));
                                      }
                         });
            _messages.Add(new ClientMessage { Message = "IGN" });
            _messages.Add(new ClientMessage { Message = "FRL" });
            _messages.Add(new ClientMessage
                         {
                             Message = "ORS",
                             Config = (id, data) =>
                                             {
                                                 var ors = JsonConvert.DeserializeObject<ClientObjects.ORS>(data);
                                                 Events.Publish(new EventMessages.PrivateChannelsReceived(ors.Channels));
                                             }
                         });
            _messages.Add(new ClientMessage { Message = "PIN", Config = (id, data) => { /* With the changes to how PIN works this no longer needs to send anything back */ } });
            _messages.Add(new ClientMessage { Message = "PRD" });
            _messages.Add(new ClientMessage
                         {
                             Message = "PRI",
                             Config = (id, data) =>
                                      {
                                          var pri = JsonConvert.DeserializeObject<ClientObjects.PRI>(data);
                                          Events.Publish(
                                              new EventMessages.NewPrivateMessage(UserManager.GetUser(pri.Character),
                                                                                  pri.Message, id));
                                      }
                         });
            _messages.Add(new ClientMessage { Message = "LRP" });
            _messages.Add(new ClientMessage { Message = "RLL" });
            _messages.Add(new ClientMessage { Message = "RMO" });
            _messages.Add(new ClientMessage { Message = "RTB" });
            _messages.Add(new ClientMessage
                         {
                             Message = "STA",
                             Config = (id, data) =>
                                      {
                                          var sta = JsonConvert.DeserializeObject<ClientObjects.STA>(data);
                                          UserManager.UpdateStatus(sta.Character, sta.Status, sta.StatusMSG);
                                          Events.Publish(
                                              new EventMessages.UserStatusChanged(UserManager.GetUser(sta.Character), id));
                                      }
                         });
            _messages.Add(new ClientMessage
                         {
                             Message = "SYS",
                             Config = (id, data) =>
                                      {
                                          var sys = JsonConvert.DeserializeObject<ClientObjects.SYS>(data);
                                          Events.Publish(new EventMessages.SystemMessage(sys.Message, sys.Channel, id));
                                      }
                         });
            _messages.Add(new ClientMessage { Message = "TPN" });
            _messages.Add(new ClientMessage { Message = "UPT" });
            _messages.Add(new ClientMessage { Message = "VAR" });
        }

        private IEventAggregator Events { get; set; }

        #region IHandle<MessageFromServer> Members

        public void Handle(EventMessages.MessageFromServer message)
        {
            var cmd = message.Message.Substring(0, 3);
            _messages.Find(x => x.Message == cmd).Config(message.ConnectionId,
                                                        message.Message.Length > 3 ? message.Message.Substring(4) : null);
        }

        #endregion

        private void HandeErrorMessage(ErrorMessages.ErrorMessage err)
        {
            switch (err.Number)
            {
                case -1:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.FatalInternalError)));
                    break;
                case -2:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CommandError)));
                    break;
                case -3:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CommandNotImplemented)));
                    break;
                case -4:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.LoginTimedOut)));
                    break;
                case -5:
                default:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.UnknownError)));
                    break;
                case -10:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.NoDiceFrontpage)));
                    break;
                case 0:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.OperationSucces)));
                    break;
                case 1:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.SyntaxError)));
                    break;
                case 2:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.NoFreeLoginSlots)));
                    break;
                case 3:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.RequiresLoggedIn)));
                    break;
                case 4:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.IdentificationFailed)));
                    break;
                case 5:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.ChannelMessageWait)));
                    break;
                case 6:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CharacterNotFound)));
                    break;
                case 7:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.ProfileRequestWait)));
                    break;
                case 8:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.UnknownCommand)));
                    break;
                case 9:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.BannedFromServer)));
                    break;
                case 10:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.AdminRequired)));
                    break;
                case 11:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.AlreadyIdentified)));
                    break;
                case 13:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.KinkRequestWait)));
                    break;
                case 15:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.MessageTooLong)));
                    break;
                case 16:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CharacterAlreadyChatOp)));
                    break;
                case 17:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CharacterNotChatOp)));
                    break;
                case 18:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.NoSearchResult)));
                    break;
                case 19:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CommandRequiresOp)));
                    break;
                case 20:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CharacterIgnoresYou)));
                    break;
                case 21:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CannotUseActionOnOp)));
                    break;
                case 26:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.ChannelNotLocated)));
                    break;
                case 28:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.AlreadyInChannel)));
                    break;
                case 30:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.TooManyIPConnections)));
                    break;
                case 31:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.LoggedInOtherLocation)));
                    break;
                case 32:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.AccountAlreadyBanned)));
                    break;
                case 33:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.UknownAuthMethod)));
                    break;
                case 36:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.DiceRollProblem)));
                    break;
                case 38:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.TimeoutOutOfRange)));
                    break;
                case 39:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.TimeoutReceived)));
                    break;
                case 40:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.KickedFromChat)));
                    break;
                case 41:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CharAlreadyChanBanned)));
                    break;
                case 42:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CharNotChanBanned)));
                    break;
                case 44:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.ChanRequiresInvite)));
                    break;
                case 45:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.MustBeInChanToSend)));
                    break;
                case 47:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CannotInvToPubChan)));
                    break;
                case 48:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.BannedFromChannel)));
                    break;
                case 49:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.CharNotFoundInChan)));
                    break;
                case 50:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.SearchWait)));
                    break;
                case 54:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.ModContactWait)));
                    break;
                case 56:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.RolePlayAdWait)));
                    break;
                case 59:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.ChanDisallowRPAd)));
                    break;
                case 60:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.ChanDisallowChat)));
                    break;
                case 61:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.TooManySearchTerms)));
                    break;
                case 62:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.NoLoginSlotsFree)));
                    break;
                case 64:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.IgnoreListFull)));
                    break;
                case 67:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.ChannelTitleTooLong)));
                    break;
                case 72:
                    Events.Publish(ErrorMessages.GenerateEventMessage(err, typeof(ErrorMessages.TooManySearchResults)));
                    break;
            }
        }
    }

    internal class ClientMessage
    {
        public Action<Guid, string> Config;
        public string Message;

        public ClientMessage()
        {
            Config = NYI;
        }

        private void NYI(Guid id, string data = null)
        {
            IoC.Get<IEventAggregator>().Publish(new EventMessages.NotImplemented(Message, id));
        }
    }
}