﻿using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;

namespace SapphireChat.Infrastructure.FChat.Users
{
    public class UserManager
    {
        private static UserManager _manager;

        public UserManager()
        {
            Users = new BindableCollection<User>();
            FriendsList = new BindableCollection<string>();
            IgnoreList = new BindableCollection<string>();
            Ops = new BindableCollection<string>();
        }

        public static UserManager Manager
        {
            get { return _manager ?? (_manager = IoC.Get<UserManager>()); }
        }

        public BindableCollection<string> FriendsList { get; set; }
        public BindableCollection<User> Users { get; set; }
        public BindableCollection<string> IgnoreList { get; set; }
        public BindableCollection<string> Ops { get; set; }

        public static User GetUser(string character)
        {
            var user = Manager.Users.SingleOrDefault(x => x.Identity.ToLower() == character.ToLower());
            if (user == null)
            {
                user = new User {Identity = character};
                AddUser(user);
            }
            return user;
        }

        public static void AddUser(User character)
        {
            if (Manager.Users.Any(x => x.Identity.ToLower() == character.Identity.ToLower()))
                return;
            Manager.Users.Add(character);
        }

        public static void AddFriends(IEnumerable<string> characters)
        {
            Manager.FriendsList.AddRange(characters);
        }

        public static void AddFriend(string character)
        {
            Manager.FriendsList.Add(character);
        }

        public static void AddRange(IEnumerable<User> characters)
        {
            foreach (var i in characters)
            {
                if (Manager.FriendsList.Any(x => x.ToLower() == i.Identity.ToLower()))
                    i.IsFriend = true;
                if (Manager.IgnoreList.Any(x => x.ToLower() == i.Identity.ToLower()))
                    i.IsIgnored = true;
                AddUser(i);
            }
        }

        public static void AddOps(IEnumerable<string> characters)
        {
            Manager.Ops.AddRange(characters);
        }

        public static void AddOp(string character)
        {
            Manager.Ops.Add(character);
            var user = GetUser(character);
            if (user != null)
                user.IsChatOp = true;
        }

        public static void RemoveOp(string character)
        {
            var user = Manager.Ops.SingleOrDefault(x => x.ToLower() == character.ToLower());
            if (user != null)
                Manager.Ops.Remove(user);
            var luser = GetUser(user);
            if (luser != null)
                luser.IsChatOp = false;
        }

        public static void AddIgnore(string character)
        {
            Manager.IgnoreList.Add(character);
        }

        public static void AddIgnores(IEnumerable<string> characters)
        {
            foreach (var i in characters)
                AddIgnore(i);
        }

        public static void RemoveIgnore(string character)
        {
            var user = Manager.IgnoreList.SingleOrDefault(x => x.ToLower() == character.ToLower());
            if (user != null)
                Manager.IgnoreList.Remove(user);
        }

        public static void RemoveUser(string character)
        {
            var user = Manager.Users.SingleOrDefault(x => x.Identity.ToLower() == character.ToLower());
            if (user != null)
                Manager.Users.Remove(user);
        }

        public static void UpdateStatus(string character, CharacterStatus status, string statusMessage)
        {
            var user = Manager.Users.SingleOrDefault(x => x.Identity.ToLower() == character.ToLower());
            if (user == null)
            {
                user = new User {Identity = character};
                AddUser(user);
            }
            user.StatusMessage = statusMessage;
            user.Status = status;
        }
    }
}