﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;
using SapphireChat.Infrastructure.FChat.Icons;
using Newtonsoft.Json.Linq;

namespace SapphireChat.Infrastructure.FChat.Users
{
    [JsonConverter(typeof (UserJsonConverter))]
    public class User : INotifyPropertyChanged, IEqualityComparer<User>
    {
        private BitmapImage _icon;
        public string Identity { get; set; }
        public Gender Gender { get; set; }
        public bool IsBookmark { get; set; }
        public bool IsFriend { get; set; }
        public CharacterStatus Status { get; set; }
        public string StatusMessage { get; set; }
        public bool IsIgnored { get; set; }
        public bool IsChatOp { get; set; }

        public BitmapImage Icon
        {
            get { return _icon ?? (_icon = IconManager.GetIcon(Identity, x => Icon = x)); }
            set
            {
                _icon = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Icon"));
            }
        }

        #region IEqualityComparer<User> Members

        public bool Equals(User x, User y)
        {
            return x.Identity.ToLower() == y.Identity.ToLower();
        }

        public int GetHashCode(User obj)
        {
            return obj.Identity.GetHashCode();
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        #endregion
    }

    public class UserJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            var result = serializer.Deserialize<JContainer>(reader) ;
            if (result is JObject)
            {
                var r = (result as JObject);
                return UserManager.GetUser(r["identity"].Value<string>());
            }
            else
            {
                var r = result.Values<string>().ToList();
                var user = UserManager.GetUser(r[0]);
                user.Gender = (Gender)Enum.Parse(typeof(Gender), r[1].Replace("-", "").Replace(" ", ""), true);
                user.Status = (CharacterStatus)Enum.Parse(typeof(CharacterStatus), r[2], true);
                user.StatusMessage = r[3];
                return user;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
        }
    }
}