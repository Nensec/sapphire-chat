﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Media.Imaging;
using Caliburn.Micro;

namespace SapphireChat.Infrastructure.FChat.Icons
{
    public class IconManager
    {
        private const string FLISTAVATARURL = "http://static.f-list.net/images/avatar/";

        private static string _iconPath =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Sapphire Chat",
                         "Icons");

        private static IconManager _manager;

        public IconManager()
        {
            Icons = new List<FChatImage>();
            if (!Directory.Exists(_iconPath))
                Directory.CreateDirectory(_iconPath);
        }

        public static IconManager Manager
        {
            get
            {
                if (_manager == null)
                    _manager = IoC.Get<IconManager>();
                return _manager;
            }
        }

        public List<FChatImage> Icons { get; private set; }

        public static BitmapImage GetIcon(string characterName, Action<BitmapImage> result,
                                          bool forceUpdate = false)
        {
            if (!forceUpdate)
            {
                // Check if we got an image memory cached.
                var localData = Manager.Icons.SingleOrDefault(x => x.Name == characterName);
                if (localData != null)
                    return localData.Image;

                // See if we got it disk cached
                var fileName = "{0}\\{1}.png".F(_iconPath, characterName);
                if (File.Exists(fileName))
                {
                    var image = new BitmapImage(new Uri(fileName));
                    Manager.Icons.Add(new FChatImage(characterName, image, new FileInfo(fileName).LastWriteTime));
                    return image;
                }
            }
            // If all else fails or forced: Get new image from server.
            using (var client = new WebClient())
            {
                var fimg = new FChatImage(characterName, null, DateTime.Now);
                Manager.Icons.Add(fimg);
                result(new BitmapImage(new Uri("\\Images\\noimage.bmp", UriKind.Relative)));
                client.DownloadDataCompleted += (s, e) =>
                                                {
                                                    var image = new BitmapImage();
                                                    image.BeginInit();
                                                    image.CacheOption = BitmapCacheOption.OnLoad;
                                                    image.StreamSource = new MemoryStream(e.Result);
                                                    image.EndInit();
                                                    fimg.Image = image;

                                                    using (
                                                        var stream =
                                                            new FileStream("{0}\\{1}.png".F(_iconPath, characterName),
                                                                           FileMode.Create, FileAccess.ReadWrite))
                                                        stream.Write(e.Result, 0, e.Result.Length);

                                                    result(image);
                                                    client.Dispose();
                                                };
                client.DownloadDataAsync(new Uri("{0}{1}.png".F(FLISTAVATARURL, characterName.ToLower())));
            }
            return null;
        }
    }
}