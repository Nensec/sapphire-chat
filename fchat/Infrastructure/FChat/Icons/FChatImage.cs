﻿using System;
using System.Windows.Media.Imaging;

namespace SapphireChat.Infrastructure.FChat.Icons
{
    public class FChatImage
    {
        public FChatImage(string name, BitmapImage image, DateTime downloadTimeStamp)
        {
            Name = name;
            Image = image;
            DownloadTimeStamp = downloadTimeStamp;
        }

        public BitmapImage Image { get; set; }
        public DateTime DownloadTimeStamp { get; private set; }
        public string Name { get; private set; }
    }
}