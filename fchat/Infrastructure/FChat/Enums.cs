﻿using System.ComponentModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SapphireChat.Infrastructure.FChat
{
    [JsonConverter(typeof (StringEnumConverter))]
    public enum Gender
    {
        Male,
        Female,
        Transgender,
        Herm,
        Shemale,
        [EnumMember(Value = "Male-Herm")] [Description("Male-Herm")] MaleHerm,
        [EnumMember(Value = "Cunt-Boy")] [Description("Cunt-Boy")] CuntBoy,
        None
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum Orientation
    {
        Straight,
        Gay,
        Bisexual,
        Asexual,
        Unsure,
        [EnumMember(Value = "Bi - male preference")] [Description("Bi - male preference")] BiMalePreference,
        [EnumMember(Value = "Bi - female preference")] [Description("Bi - female preference")] BiFemalePreference,
        Pansexual,
        [EnumMember(Value = "Bi-curious")] [Description("Bi-curious")] BiCurious
    }

    public enum Language
    {
        Dutch,
        English,
        French,
        Spanish,
        German,
        Russian,
        Chinese,
        Japanese,
        Portuguese,
        Korean,
        Arabic,
        Italian,
        Swedish,
        Other
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum FurryPref
    {
        [EnumMember(Value = "No furry characters, just humans")] [Description("No furry characters, just humans")] NoFurryCharactersJustHumans,
        [EnumMember(Value = "No humans, just furry characters")] [Description("No humans, just furry characters")] NoHumansJustFurryCharacters,
        [EnumMember(Value = "Furries ok, Humans Preferred")] [Description("Furries ok, Humans Preferred")] FurriesOkHumansPreferred,
        [EnumMember(Value = "Humans ok, Furries Preferred")] [Description("Humans ok, Furries Preferred")] HumansOkFurriesPreferred,
        [EnumMember(Value = "Furs and / or humans")] [Description("Furs and / or humans")] FursAndOrHumans
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum Role
    {
        [EnumMember(Value = "Always dominant")] [Description("Always dominant")] AlwaysDominant,
        [EnumMember(Value = "Usually dominant")] [Description("Usually dominant")] UsuallyDominant,
        Switch,
        [EnumMember(Value = "Usually submissive")] [Description("Usually submissive")] UsuallySubmissive,
        [EnumMember(Value = "Always submissive")] [Description("Always submissive")] AlwaysSubmissive,
        None
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum ChannelMode
    {
        [EnumMember(Value = "chat")] [Description("chat")] Chat,
        [EnumMember(Value = "ads")] [Description("ads")] ADs,
        [EnumMember(Value = "both")] [Description("both")] Both
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum IgnoreAction
    {
        [EnumMember(Value = "add")] Add,
        [EnumMember(Value = "delete")] Delete,
        [EnumMember(Value = "notify")] Notify,
        [EnumMember(Value = "list")] List,
        [EnumMember(Value = "init")] Init
    }


    [JsonConverter(typeof (StringEnumConverter))]
    public enum RoomStatus
    {
        [EnumMember(Value = "open")] Open,
        [EnumMember(Value = "closed")] Closed
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum CharacterStatus
    {
        [EnumMember(Value = "online")] Online,
        [EnumMember(Value = "looking")] Looking,
        [EnumMember(Value = "away")] Away,
        [EnumMember(Value = "idle")] Idle,
        [EnumMember(Value = "busy")] Busy,
        [EnumMember(Value = "dnd")] DND,
        [EnumMember(Value = "crown")] Crown
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum TypingStatus
    {
        [EnumMember(Value = "clear")] Clear,
        [EnumMember(Value = "paused")] Paused,
        [EnumMember(Value = "typing")] Typing
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum RLLType
    {
        [EnumMember(Value = "dice")] Dice,
        [EnumMember(Value = "bottle")] Bottle
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum KIDType
    {
        [EnumMember(Value = "start")] Start,
        [EnumMember(Value = "custom")] Custom,
        [EnumMember(Value = "end")] End
    }

    [JsonConverter(typeof (StringEnumConverter))]
    public enum PRDType
    {
        [EnumMember(Value = "start")] Start,
        [EnumMember(Value = "info")] Info,
        [EnumMember(Value = "select")] Select,
        [EnumMember(Value = "end")] End
    }
}