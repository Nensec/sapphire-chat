﻿using System;
using System.Collections.Generic;
using System.Linq;
using SapphireChat.Infrastructure.FChat.Users;

namespace SapphireChat.Infrastructure.FChat
{
    public class ConnectionManager
    {
        private static List<ServerCommands> Connections = new List<ServerCommands>();

        public static List<User> LoggedInUsers
        {
            get
            {
                return
                    Connections.Where(x => x.IsConnected && x.IdentifiedAs != null).Select(x => x.IdentifiedAs).ToList();
            }
        }

        public static ServerCommands GetConnection(Guid id)
        {
            if (id.Equals(Guid.Empty))
                return Connections.First();
            return Connections.Single(x => x.ConnectionId == id);
        }

        public static ServerCommands GetConnection()
        {
            return Connections.FirstOrDefault(x => x.IsConnected && x.IdentifiedAs != null);
        }

        public static ServerCommands GetConnection(string characterName)
        {
            return Connections.Single(x => x.IdentifiedAs != null && x.IdentifiedAs.Identity == characterName);
        }

        public static ServerCommands GetLastActiveConnection()
        {
            return GetConnection(ServerCommands.LastActiveConnectionId);
        }

        public static ServerCommands NewConnection()
        {
            var cmds = new ServerCommands();
            Connections.Add(cmds);
            return cmds;
        }

        public static List<ServerCommands> HealthyConnections()
        {
            return Connections.Where(x => x.IsConnected && x.IdentifiedAs != null).ToList();
        }

        public static void CloseConnection(Guid id)
        {
            var cmds = Connections.Single(x => x.ConnectionId == id);
            cmds.Disconnect();
        }

        public static void KillAll()
        {
            Connections.ForEach(x => x.Disconnect());
        }
    }
}