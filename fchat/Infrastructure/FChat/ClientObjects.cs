﻿using SapphireChat.Infrastructure.FChat.Channels;
using SapphireChat.Infrastructure.FChat.Users;

namespace SapphireChat.Infrastructure.FChat
{
    public class ClientObjects
    {
        #region Nested type: ADL

        public class ADL : IClientObject
        {
            public string[] Ops { get; set; }
        }

        #endregion

        #region Nested type: AOP

        public class AOP : IClientObject
        {
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: APITicket

        public class APITicket : IClientObject
        {
            public string[] Characters { get; set; }
            public string Default_Character { get; set; }
            public string Account_Id { get; set; }
            public string Ticket { get; set; }
            public Friend[] Friends { get; set; }
            public Bookmark[] Bookmarks { get; set; }
            public string Error { get; set; }
        }

        #endregion

        #region Nested type: BRO

        public class BRO : IClientObject
        {
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: Bookmark

        public class Bookmark
        {
            public string Name { get; set; }
        }

        #endregion

        #region Nested type: CBU

        public class CBU : IClientObject
        {
            public string Operator { get; set; }
            public string Channel { get; set; }
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: CDS

        public class CDS : IClientObject
        {
            public string Channel { get; set; }
            public string Description { get; set; }
        }

        #endregion

        #region Nested type: CHA

        public class CHA : IClientObject
        {
            public Channel[] Channels { get; set; }
        }

        #endregion

        #region Nested type: CIU

        public class CIU : IClientObject
        {
            public string Sender { get; set; }
            public string Title { get; set; }
            public string Name { get; set; }
        }

        #endregion

        #region Nested type: CKU

        public class CKU : IClientObject
        {
            public string Operator { get; set; }
            public string Channel { get; set; }
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: COL

        public class COL : IClientObject
        {
            public string Channel { get; set; }
            public string[] OpList { get; set; }
        }

        #endregion

        #region Nested type: CON

        public class CON : IClientObject
        {
            public int Count { get; set; }
        }

        #endregion

        #region Nested type: CSO

        public class CSO : IClientObject
        {
            public string Characte { get; set; }
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: DOP

        public class DOP : IClientObject
        {
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: ERR

        public class ERR : IClientObject
        {
            public string Message { get; set; }
            public int Number { get; set; }
        }

        #endregion

        #region Nested type: FKS

        public class FKS : IClientObject
        {
            public string[] Characters { get; set; }
            public int[] Kinks { get; set; }
        }

        #endregion

        #region Nested type: FLN

        public class FLN : IClientObject
        {
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: FRL

        public class FRL : IClientObject
        {
            public string[] Characters { get; set; }
        }

        #endregion

        #region Nested type: Friend

        public class Friend
        {
            public string Source_Name { get; set; }
            public string Dest_Name { get; set; }
        }

        #endregion

        #region Nested type: HLO

        public class HLO : IClientObject
        {
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: ICH

        public class ICH : IClientObject
        {
            public User[] Users { get; set; }
            public string Channel { get; set; }
            public string Title { get; set; }
            public ChannelMode Mode { get; set; }
        }

        #endregion

        #region Nested type: IDN

        public class IDN : IClientObject
        {
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: IGN

        public class IGN : IClientObject
        {
            public string[] Characters { get; set; }
            public IgnoreAction Action { get; set; }
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: JCH

        public class JCH : IClientObject
        {
            public string Channel { get; set; }
            public string Title { get; set; }
            public User Character { get; set; }
        }

        #endregion

        #region Nested type: KID

        public class KID : IClientObject
        {
            public KIDType Type { get; set; }
            public string Message { get; set; }
            public int[] Key { get; set; }
            public int[] Value { get; set; }
        }

        #endregion

        #region Nested type: LCH

        public class LCH : IClientObject
        {
            public string Channel { get; set; }
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: LIS

        public class LIS : IClientObject
        {
            public User[] Characters { get; set; }
        }

        #endregion

        #region Nested type: LRP

        public class LRP : IClientObject
        {
            public string Character { get; set; }
            public string Channel { get; set; }
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: MSG

        public class MSG : IClientObject
        {
            public string Character { get; set; }
            public string Message { get; set; }
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: NLN

        public class NLN : IClientObject
        {
            public CharacterStatus Status { get; set; }
            public string Identity { get; set; }
            public Gender Gender { get; set; }
        }

        #endregion

        #region Nested type: ORS

        public class ORS : IClientObject
        {
            public Channel[] Channels { get; set; }
        }

        #endregion

        #region Nested type: PRD

        public class PRD : IClientObject
        {
            public PRDType Type { get; set; }
            public string Message { get; set; }
            public string Key { get; set; }
            public string Value { get; set; }
        }

        #endregion

        #region Nested type: PRI

        public class PRI : IClientObject
        {
            public string Character { get; set; }
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: RLL

        public class RLL : IClientObject
        {
            public string Channel { get; set; }
            public int[] Results { get; set; }
            public RLLType Type { get; set; }
            public string[] Rolls { get; set; }
            public string Target { get; set; }
        }

        #endregion

        #region Nested type: RMO

        public class RMO : IClientObject
        {
            public ChannelMode Mode { get; set; }
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: RTB

        public class RTB : IClientObject
        {
            public string Type { get; set; }
            public string Character { get; set; }
        }

        #endregion

        #region Nested type: STA

        public class STA : IClientObject
        {
            public CharacterStatus Status { get; set; }
            public string Character { get; set; }
            public string StatusMSG { get; set; }
        }

        #endregion

        #region Nested type: SYS

        public class SYS : IClientObject
        {
            public string Message { get; set; }
            public string Channel { get; set; }
        }

        #endregion

        #region Nested type: TPN

        public class TPN : IClientObject
        {
            public string Character { get; set; }
            public TypingStatus Status { get; set; }
        }

        #endregion

        #region Nested type: UPT

        public class UPT : IClientObject
        {
            public int Time { get; set; }
            public int StartTime { get; set; }
            public string StartString { get; set; }
            public int Accepted { get; set; }
            public int Channels { get; set; }
            public int Users { get; set; }
            public int MaxUsers { get; set; }
        }

        #endregion

        #region Nested type: VAR

        public class VAR : IClientObject
        {
            public string Variable { get; set; }
            public double Value { get; set; }
        }

        #endregion
    }

    public interface IClientObject
    {
    }
}