﻿using System;
using System.Collections.Specialized;
using System.Deployment.Application;
using System.Net;
using System.Reflection;
using System.Text;
using Caliburn.Micro;
using Newtonsoft.Json;
using Ninject;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat.Users;
using SapphireChat.Infrastructure.Log;
using SapphireChat.Properties;
using WebSocket4Net;
using System.Timers;

namespace SapphireChat.Infrastructure.FChat
{
    /// <summary>
    /// Messages handled by the server, send by the client
    /// </summary>
    public class ServerCommands : IHandle<EventMessages.ServerSettingChanged>
    {
        private string _account;
        private const string ChatClientName = "SapphireChat";

        private readonly string _chatClientVersion = ApplicationDeployment.IsNetworkDeployed
                                                ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
                                                : Assembly.GetExecutingAssembly().GetName().Version.ToString();

        private IEventAggregator _events;
        private bool _plannedDc;
        private DateTime _ticketReceived;
        private WebSocket _webSocket;
        private readonly Timer _pingTimer = new Timer(30000);

        public static Guid LastActiveConnectionId { get; set; }

        public ServerCommands()
        {
            ConnectionId = Guid.NewGuid();
            _pingTimer.Elapsed += PingTimerElapsed;
        }

        void PingTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if(IsConnected)
                PingServer();
            else
                _pingTimer.Stop();
        }

        public ClientObjects.APITicket Ticket { get; private set; }

        public bool IsConnected
        {
            get { return _webSocket != null && _webSocket.State == WebSocketState.Open; }
        }

        private bool IsConnecting
        {
            get { return _webSocket != null && _webSocket.State == WebSocketState.Connecting; }
        }

        public User IdentifiedAs { get; set; }
        public ViewModels.Interfaces.IContent ActiveWindow { get; set; }
        // Perhaps can be done better.. but this allows the ChatProcessor to know what window it should act on for say /who

        public Guid ConnectionId { get; private set; }

        [Inject]
        public IEventAggregator Events
        {
            get { return _events; }
            set
            {
                _events = value;
                _events.Subscribe(this);
            }
        }

        #region IHandle<ServerSettingChanged> Members

        public void Handle(EventMessages.ServerSettingChanged message)
        {
            if (!IsConnecting || !IsConnected)
                CreateWebsocket();
        }

        #endregion

        private void CreateWebsocket()
        {
            var address = "w{0}://{1}:{2}".F(Settings.Default.UseSSL ? "ss" : "s", Settings.Default.FCHATADDRESS,
                                             Settings.Default.UseTestServer
                                                 ? Settings.Default.SSLTESTPORT
                                                 : Settings.Default.SSLPORT);
            _webSocket = new WebSocket(address) {EnableAutoSendPing = false};
            _webSocket.Opened += WebSocketOpened;
            _webSocket.Closed += WebSocketClosed;
            _webSocket.Error += WebSocketError;
            _webSocket.MessageReceived += WebSocketMessageReceived;
        }

        private void WebSocketMessageReceived(object sender, MessageReceivedEventArgs e)
        {
            Events.Publish(new EventMessages.MessageFromServer(e, ConnectionId));
        }

        private void WebSocketError(object sender, SuperSocket.ClientEngine.ErrorEventArgs e)
        {
            Events.Publish(new EventMessages.WebsocketError(e, ConnectionId));
        }

        private void WebSocketClosed(object sender, EventArgs e)
        {
            _pingTimer.Stop();
            Events.Publish(new EventMessages.DisconnectedFromServer(ConnectionId, "", _plannedDc));
        }

        private void WebSocketOpened(object sender, EventArgs e)
        {
            _pingTimer.Start();
            Events.Publish(new EventMessages.ConnectedToServer(ConnectionId));
        }

        private void WebSocketSend(string type, IServerObject data, Type[] longRunningEventEnd = null, string message = null,
                                   System.Action cancelAction = null, int maxStep = 0, int currentStep = 0)
        {
            if (IsConnected && (IdentifiedAs != null || data is ServerObjects.IDN))
            {
                if (longRunningEventEnd != null)
                    Events.Publish(new EventMessages.LongRunningProcess(longRunningEventEnd, message, cancelAction,
                                                                        ConnectionId, maxStep, currentStep));
                if (data == null)
                {
                    _webSocket.Send(type);
                    DebugLog.LogMessage("Send message to server: {0}", type);
                }
                else
                {
                    var msg = "{0} {1}".F(type, JsonConvert.SerializeObject(data));
                    _webSocket.Send(msg);
                    DebugLog.LogMessage("Send message to server: {0}", msg);
                }
            }
        }

        public void Identify(string character)
        {
            WebSocketSend("IDN",
                          new ServerObjects.IDN
                              {
                                  Account = _account,
                                  Character = character,
                                  CName = ChatClientName,
                                  CVersion = _chatClientVersion,
                                  Ticket = Ticket.Ticket
                              },
                          new[]
                              {
                                  typeof (EventMessages.IdentifySuccess),
                                  typeof (ErrorMessages.IdentificationFailed)
                              },
                          "Identifying..", () => _webSocket.Close());
        }

        public void Connect(string username, string password)
        {
            DebugLog.LogMessage("Connecting to server..");
            if (_account == username && _ticketReceived.AddDays(1) > DateTime.Now)
            {
                if (_webSocket == null)
                    CreateWebsocket();
                if (!IsConnected && !IsConnecting)
                {
                    Events.Publish(new EventMessages.LongRunningProcess(
                                       new[] { typeof(EventMessages.ConnectedToServer) }, "Connecting to server..",
                                       () => _webSocket.Close(), ConnectionId));

                    if (_webSocket != null) _webSocket.Open();
                }
            }
            else
            {
                DebugLog.LogMessage("No ticket! Requesting ticket..");
                RequestTicket(username, password);
            }
        }

        public void Disconnect()
        {
            if (IsConnected)
            {
                _plannedDc = true;
                IdentifiedAs = null;
                _webSocket.Close("Logout");
            }
        }

        private void RequestTicket(string username, string password)
        {
            Events.Publish(
                new EventMessages.LongRunningProcess(
                    new[] {typeof (EventMessages.LoginFailure), typeof (EventMessages.TicketReceived)},
                    "Retrieving login ticket..", null, ConnectionId));
            _account = username;
            using (var client = new WebClient())
            {
                var vals = new NameValueCollection {{"account", username}, {"password", password}};
                DebugLog.LogMessage("Send ticket request to server for account: {0}", username);
                try
                {
                    var result = client.UploadValues("http://www.f-list.net/json/getApiTicket.php", vals);

                    Ticket = JsonConvert.DeserializeObject<ClientObjects.APITicket>(Encoding.ASCII.GetString(result));
                    if (Ticket.Error != "")
                        Events.Publish(new EventMessages.LoginFailure(Ticket.Error, ConnectionId));
                    else
                    {
                        _ticketReceived = DateTime.Now;
                        Events.Publish(new EventMessages.TicketReceived(Ticket, ConnectionId));
                        Connect(username, password);
                    }
                }
                catch (Exception ex)
                {
                    Events.Publish(new EventMessages.LoginFailure(ex.Message, ConnectionId));
                }
            }
        }

        public void PingServer()
        {
            WebSocketSend("PIN", null);
        }

        public void GlobalBanUser(string character)
        {
            WebSocketSend("ACB", new ServerObjects.ACB {Character = character});
        }

        public void GlobalOpUser(string character)
        {
            WebSocketSend("AOP", new ServerObjects.AOP {Character = character});
        }

        public void RequestConnectedCharacters(string character)
        {
            WebSocketSend("AWC", new ServerObjects.AWC {Character = character});
        }

        public void BroadcastMessage(string message)
        {
            WebSocketSend("BRO", new ServerObjects.BRO {Message = message});
        }

        public void RequestBanList(string channel)
        {
            WebSocketSend("CBL", new ServerObjects.CBL {Channel = channel});
        }

        public void ChannelBanUser(string character, string channel)
        {
            WebSocketSend("CBU", new ServerObjects.CBU {Character = character, Channel = character});
        }

        public void CreateChannel(string title)
        {
            WebSocketSend("CCR", new ServerObjects.CCR {Channel = title});
        }

        public void ChangeChannelDescription(string channel, string description)
        {
            WebSocketSend("CDS", new ServerObjects.CDS {Channel = channel, Description = description});
        }

        public void RequestPublicChannels()
        {
            WebSocketSend("CHA", null);
        }

        public void SendChannelInvite(string channel, string character)
        {
            WebSocketSend("CIU", new ServerObjects.CIU {Character = character, Channel = channel});
        }

        public void KickFromChannel(string channel, string character)
        {
            WebSocketSend("CKU", new ServerObjects.CKU {Character = character, Channel = channel});
        }

        public void ChannelOpCharacter(string channel, string character)
        {
            WebSocketSend("COA", new ServerObjects.COA {Character = character, Channel = channel});
        }

        public void RequestChannelOps(string channel)
        {
            WebSocketSend("COL", new ServerObjects.COL {Channel = channel});
        }

        public void RemoveChannelOp(string channel, string character)
        {
            WebSocketSend("COR", new ServerObjects.COR {Character = character, Channel = channel});
        }

        public void CreateOfficialChannel(string channel)
        {
            WebSocketSend("CRC", new ServerObjects.CRC {Channel = channel});
        }

        public void SetChannelOwner(string channel, string character)
        {
            WebSocketSend("CSO", new ServerObjects.CSO {Character = character, Channel = channel});
        }

        public void ChannelTimeoutUser(string channel, string character, TimeSpan time)
        {
            WebSocketSend("CTU",
                          new ServerObjects.CTU
                          {Channel = channel, Character = character, Length = (int) time.TotalMinutes});
        }

        public void ChannelUnbanUser(string channel, string character)
        {
            WebSocketSend("CUB", new ServerObjects.CUB {Channel = channel, Character = character});
        }

        public void RemoveGlobalOp(string character)
        {
            WebSocketSend("DOP", new ServerObjects.DOP {Character = character});
        }

        public void FindKinkyUsers(int[] kinks, Gender[] genders, Orientation[] orientations, Language[] languages,
                                   FurryPref[] furryPrefs, Role[] roles)
        {
            WebSocketSend("FKS",
                          new ServerObjects.FKS
                          {
                              Kinks = kinks,
                              Genders = genders,
                              Orientations = orientations,
                              Languages = languages,
                              FurryPrefs = furryPrefs,
                              Roles = roles
                          });
        }

        public void IgnoreListAction(IgnoreAction action, string character)
        {
            WebSocketSend("IGN", new ServerObjects.IGN {Action = action, Character = character});
        }

        public void JoinChannel(string channelName)
        {
            WebSocketSend("JCH", new ServerObjects.JCH {Channel = channelName});
        }

        public void KickUserFromServer(string character)
        {
            WebSocketSend("KIK", new ServerObjects.KIK {Character = character});
        }

        public void LeaveChannel(string channel)
        {
            WebSocketSend("LCH", new ServerObjects.LCH {Channel = channel});
        }

        public void SendChannelMessage(string channel, string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
                WebSocketSend("MSG", new ServerObjects.MSG {Channel = channel, Message = message});
        }

        public void RequestPrivateChannels()
        {
            WebSocketSend("ORS", null);
        }

        public void SendPrivateMessage(string character, string message)
        {
            if(!string.IsNullOrWhiteSpace(message))
                WebSocketSend("PRI", new ServerObjects.PRI {Recipient = character, Message = message});
        }

        public void RequestProfileInfo(string character)
        {
            WebSocketSend("PRO", new ServerObjects.PRO {Character = character});
        }

        public void Roll(string channel, string dice)
        {
            WebSocketSend("RLL", new ServerObjects.RLL {Channel = channel, Dice = dice});
        }

        public void ReloadServerConfig(string save)
        {
            WebSocketSend("RLD", new ServerObjects.RLD {Save = save});
        }

        public void ChangeRoomMode(string channel, ChannelMode mode)
        {
            WebSocketSend("RMO", new ServerObjects.RMO {Channel = channel, Mode = mode});
        }

        public void ChangeRoomStatus(string channel, RoomStatus status)
        {
            WebSocketSend("RST", new ServerObjects.RST {Channel = channel, Status = status});
        }

        public void RewardUser(string charater)
        {
            WebSocketSend("RWD", new ServerObjects.RWD {Character = charater});
        }

        public void AlertAdmins(string report, string character)
        {
            WebSocketSend("SFC", new ServerObjects.SFC {Character = character, Report = report});
        }

        public void ChangeStatus(CharacterStatus status, string message)
        {
            WebSocketSend("STA", new ServerObjects.STA {Status = status, StatusMSG = message});
        }

        public void TimeoutUser(string character, TimeSpan time, string reason)
        {
            WebSocketSend("TMO",
                          new ServerObjects.TMO {Character = character, Time = (int) time.TotalMinutes, Reason = reason});
        }

        public void SendTypingStatus(string character, TypingStatus status)
        {
            WebSocketSend("TPN", new ServerObjects.TPN {Character = character, Status = status});
        }

        public void RequestServerInfo()
        {
            WebSocketSend("UPT", null);
        }
    }
}