﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using SilverFlow.Controls;

namespace SapphireChat.Infrastructure
{
    public class SapphireWindowManager : ISapphireWindowManager
    {
        #region ISapphireWindowManager Members

        [Obsolete]
        public bool? ShowDialog(object rootModel, object context = null, IDictionary<string, object> settings = null)
        {
            throw new NotImplementedException();
        }

        [Obsolete]
        public void ShowPopup(object rootModel, object context = null, IDictionary<string, object> settings = null)
        {
            throw new NotImplementedException();
        }

        public void ShowWindow(object rootModel, object context = null, IDictionary<string, object> settings = null)
        {
            new WindowManager().ShowWindow(rootModel, context, settings);
        }

        public FloatingWindow CreateDialog(IScreen vm, WindowOptions options)
        {
            var window = NewWindow(vm, options);
            window.ShowModal();
            return window;
        }

        public FloatingWindow CreatePopup(IScreen vm, WindowOptions options)
        {
            var window = NewWindow(vm, options);
            window.Show();
            return window;
        }

        #endregion

        private FloatingWindow NewWindow(IScreen vm, WindowOptions options)
        {
            var host = FindHost();

            var existingWindow = host.FloatingWindows.FirstOrDefault(x => x.Tag != null && x.Tag.Equals(options.Id));
            if (existingWindow != null)
                return existingWindow;

            var window = new FloatingWindow
                         {
                             ShowMinimizeButton = false,
                             ResizeEnabled = options.CanResize,
                             Title = options.Title,
                             Tag = options.Id
                         };

            var view = ViewLocator.LocateForModel(vm, null, null);
            ViewModelBinder.Bind(vm, view, null);

            var content = (view as System.Windows.Controls.Control);
            window.MinHeight = content.MinHeight + 20;
            window.MinWidth = content.MinWidth + 5;
            if (options.Height == 0d && content.Height != 0d)
                window.Height = content.Height + 20;
            else if (options.Height != 0d)
                window.Height = options.Height > window.MinHeight ? options.Height : window.MinHeight;
            else
                window.Height = 350;
            if (options.Width == 0d && content.Width != 0d)
                window.Width = content.Width + 5;
            else if (options.Width != 0d)
                window.Width = options.Width > window.MinWidth ? options.Width : window.MinWidth;
            else
                window.Width = 500;

            window.Content = view;

            host.Add(window);
            return window;
        }

        private FloatingWindowHost FindHost()
        {
            var host = LogicalTreeHelper.FindLogicalNode(Application.Current.MainWindow, "Host") as FloatingWindowHost;
            if (host == null)
                throw new Exception("Couldn't find the window host?");
            return host;
        }

        #region Nested type: WindowOptions

        public class WindowOptions
        {
            public WindowOptions()
            {
                CanResize = true;
            }

            public string Title { get; set; }
            public bool CanPopout { get; set; }
            public double Width { get; set; }
            public double Height { get; set; }
            public string Id { get; set; }
            public bool CanResize { get; set; }
        }

        #endregion
    }

    public interface ISapphireWindowManager : IWindowManager
    {
        FloatingWindow CreateDialog(IScreen vm, SapphireWindowManager.WindowOptions options);
        FloatingWindow CreatePopup(IScreen vm, SapphireWindowManager.WindowOptions options);
    }
}