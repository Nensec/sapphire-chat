﻿using Caliburn.Micro;
using NLog;
using Ninject;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Properties;

namespace SapphireChat.Infrastructure.Log
{
    public class DebugLog : IHandle<EventMessages.TicketReceived>,
                            IHandle<EventMessages.ConnectedToServer>,
                            IHandle<EventMessages.DisconnectedFromServer>,
                            IHandle<EventMessages.WebsocketError>,
                            IHandle<EventMessages.MessageFromServer>,
                            IHandle<ErrorMessages.ErrorMessage>
    {
        public Logger Log = NLog.LogManager.GetCurrentClassLogger();

        private IEventAggregator _events;

        [Inject]
        public IEventAggregator Events
        {
            get { return _events; }
            set
            {
                _events = value;
                if (Settings.Default.UseDebug)
                    _events.Subscribe(this);
            }
        }

        #region IHandle<ConnectedToServer> Members

        public void Handle(EventMessages.ConnectedToServer message)
        {
            Log.Debug("Connected to server");
        }

        #endregion

        #region IHandle<DisconnectedFromServer> Members

        public void Handle(EventMessages.DisconnectedFromServer message)
        {
            Log.Debug("Disconnected from server");
        }

        #endregion

        #region IHandle<ErrorMessage> Members

        public void Handle(ErrorMessages.ErrorMessage message)
        {
            Log.Debug("Error from server: ({0}) {1}", message.Number, message.Message);
        }

        #endregion

        // This will get disabled eventually, as it contains all the stuff not allowed to log too~
        // Will be replaced with more specific handling of messages

        #region IHandle<MessageFromServer> Members

        public void Handle(EventMessages.MessageFromServer message)
        {
            Log.Debug("Message from server: {0}", message.Message);
        }

        #endregion

        #region IHandle<TicketReceived> Members

        public void Handle(EventMessages.TicketReceived message)
        {
            Log.Debug("Ticket received: {0}", message.Ticket.Ticket);
        }

        #endregion

        #region IHandle<WebsocketError> Members

        public void Handle(EventMessages.WebsocketError message)
        {
            Log.ErrorException("Websocket exception", message.Error.Exception);
        }

        #endregion

        public static void LogMessage(string msg, params object[] args)
        {
            if (Settings.Default.UseDebug)
                IoC.Get<DebugLog>().Log.Debug(msg, args);
        }
    }
}