﻿using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace SapphireChat.Infrastructure.Behaviors
{
    public class ItemsControlScrollOnNewItem : Behavior<ItemsControl>
    {
        private bool isScrollDownEnabled;
        private ScrollViewer scrollViewer;

        protected override void OnAttached()
        {
            AssociatedObject.Loaded += OnLoaded;
            AssociatedObject.Unloaded += OnUnLoaded;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.Loaded -= OnLoaded;
            AssociatedObject.Unloaded -= OnUnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            var incc = AssociatedObject.ItemsSource as INotifyCollectionChanged;
            if (incc == null) return;

            incc.CollectionChanged += OnCollectionChanged;
            scrollViewer =
                AssociatedObject.GetSelfAndAncestors().Where(a => a.GetType().Equals(typeof (ScrollViewer))).
                    FirstOrDefault() as ScrollViewer;
        }

        private void OnUnLoaded(object sender, RoutedEventArgs e)
        {
            var incc = AssociatedObject.ItemsSource as INotifyCollectionChanged;
            if (incc == null) return;

            incc.CollectionChanged -= OnCollectionChanged;
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (scrollViewer != null)
            {
                isScrollDownEnabled = scrollViewer.ScrollableHeight > 0 &&
                                      scrollViewer.VerticalOffset + scrollViewer.ViewportHeight <
                                      scrollViewer.ExtentHeight;

                if (e.Action == NotifyCollectionChangedAction.Add && !isScrollDownEnabled)
                {
                    int count = AssociatedObject.Items.Count;
                    if (count == 0)
                        return;

                    //var item = AssociatedObject.Items[count - 1];

                    //var frameworkElement = AssociatedObject.ItemContainerGenerator.ContainerFromItem(item) as FrameworkElement;
                    //if (frameworkElement == null) return;
                    scrollViewer.ScrollToBottom();

                    //frameworkElement.BringIntoView();
                }
            }
        }
    }
}