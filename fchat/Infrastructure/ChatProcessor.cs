﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using SapphireChat.Infrastructure.Events;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.FChat.Channels;
using SapphireChat.Infrastructure.FChat.Users;
using SapphireChat.ViewModels;
using System.Text;
using SapphireChat.ViewModels.Interfaces;

namespace SapphireChat.Infrastructure
{
    public static class ChatProcessor
    {
        private static Dictionary<string, ChatCommand> Commands =
            new Dictionary<string, ChatCommand>();

        static ChatProcessor()
        {
            DefaultCommands();
        }

        public static IEventAggregator Events
        {
            get { return IoC.Get<IEventAggregator>(); }
        }

        public static ISapphireWindowManager WindowManager
        {
            get { return IoC.Get<ISapphireWindowManager>(); }
        }

        public static bool ProcessCommand(string input, Guid connectionId)
        {
            // /command "param with space" paramwithoutspace "another param with space" if this would be the last param then no quotes are needed and spaces are allowed!
            var temp = input.Replace("\\\"", "#QUOTE"); // Find all the 'real' quotes
            string command;
            if (temp.Contains(' ')) // contains params
            {
                command = temp.Substring(1, temp.IndexOf(' ') - 1);
                temp = temp.Substring(command.Length + 2);
            }
            else
                command = temp.Substring(1);
            ChatCommand cmd;
            if (!Commands.TryGetValue(command, out cmd))
            {
                // todo: Popup a toast or something
                return false;
            }
                
            var parameters = new List<string>();
            while (cmd.ParamCount != 0) // todo: change to for loop?
            {
                var quotecount = temp.Count(x => x == '"'); // Count all formatting quotes
                var paramsplit = quotecount >= 2 && temp.IndexOf('"') == 0
                                      ? temp.Split(new[] {'"'}, 2, StringSplitOptions.RemoveEmptyEntries).ToList()
                                      : temp.Split(new[] {' '}, 3, StringSplitOptions.RemoveEmptyEntries).ToList();
                parameters.Add(paramsplit[0].Replace("#QUOTE", "\"")); // Add the parameter to the list, make sure to put the real quote back
                paramsplit.RemoveAt(0); // Remove the used parameter from the string
                if (paramsplit.Count == 0) // No parameters left to parse
                    break;
                temp = string.Join(" ", paramsplit);
                if (parameters.Count == cmd.ParamCount) // If we got enough parameters then anything we got left is added to the end of the last parameter
                {
                    parameters[parameters.Count - 1] += " " + temp.Replace("#QUOTE", "\"");
                    break;
                }
                if (temp == "") // Failsafe, should never get this to be true
                    break;
            }

            if (parameters.Count != cmd.ParamCount)
            {
                // todo: We don't have enough paramters or they are not correctly put in for some reason, popup a toast
                return false;
            }

            cmd.Action(parameters);
            return true;
        }

        private static void DefaultCommands()
        {
            Commands.Add("logout", new ChatCommand(0, prms => Logout()));
            Commands.Add("login", new ChatCommand(2, prms => Login(prms[0], prms[1])));

            Commands.Add("joinchannel", new ChatCommand(1, prms => JoinChannel(prms[0])));
            Commands.Add("jc", new ChatCommand(1, prms => JoinChannel(prms[0])));
            Commands.Add("chan", new ChatCommand(1, prms => JoinChannel(prms[0])));
            Commands.Add("channel", new ChatCommand(1, prms => JoinChannel(prms[0])));
            Commands.Add("join", new ChatCommand(1, prms => JoinChannel(prms[0])));

            Commands.Add("priv", new ChatCommand(2, prms => SendPrivateMessage(prms[0], prms[1])));
            Commands.Add("pm", new ChatCommand(2, prms => SendPrivateMessage(prms[0], prms[1])));
            Commands.Add("w", new ChatCommand(2, prms => SendPrivateMessage(prms[0], prms[1])));
            Commands.Add("whisper", new ChatCommand(2, prms => SendPrivateMessage(prms[0], prms[1])));
            Commands.Add("tell", new ChatCommand(2, prms => SendPrivateMessage(prms[0], prms[1])));
            Commands.Add("private", new ChatCommand(2, prms => SendPrivateMessage(prms[0], prms[1])));
            Commands.Add("im", new ChatCommand(2, prms => SendPrivateMessage(prms[0], prms[1])));

            Commands.Add("status", new ChatCommand(2, prms => SetStatus(prms[0], prms[1])));

            Commands.Add("who",  new ChatCommand(1, prms => GetUserList(prms[0])));

            Commands.Add("me", new ChatCommand(1, prms => (ConnectionManager.GetLastActiveConnection().ActiveWindow as ChatWindow).DefaultMessageHandler("/me {0}".F(prms[0]))));
        }

        /// <summary>
        /// Adds a command to the processor
        /// </summary>
        /// <param name="command">The command to listen to, must not already exist. Without the /</param>
        /// <param name="paramCount">The amount of parameters this command needs</param>
        /// <param name="action">The action that needs to happen, containing parameters</param>
        /// <returns>Returns true if the command was succesfully added, false otherwise. Todo: add reasoning</returns>
        public static bool AddCommand(string command, int paramCount, Action<List<string>> action)
        {
            if (!Commands.ContainsKey(command) && !command.Contains(' '))
            {
                Commands.Add(command, new ChatCommand(paramCount, action));
                return true;
            }
            return false;
        }

        private static void GetUserList(string channelName)
        {
            var currentWindow = ConnectionManager.GetLastActiveConnection().ActiveWindow;
            StringBuilder b;
            if (currentWindow is ChatChannelViewModel && channelName == "/who") // if paramlist is empty, first param is command
            {
                b = new StringBuilder("Users in {0}: ".F((currentWindow as ChatChannelViewModel).Channel.Title ?? (currentWindow as ChatChannelViewModel).Channel.Name));
                foreach (var i in (currentWindow as ChatChannelViewModel).Channel.Users)
                    b.Append("[user]{0}[/user]".F(i));
                currentWindow.AddLine(b.ToString(), null);
            }
            else
            {
                var channels = ChannelManager.GetJoinedChannelsForConnection(currentWindow.ConnectionId != Guid.Empty ? currentWindow.ConnectionId : ServerCommands.LastActiveConnectionId);
                if (channels.Any())
                {
                    var chanCount = channels.Count(x => x.Name == channelName || x.Title == channelName);
                    var chan = channels.Single(x => x.Name == channelName || x.Title == channelName);
                    if (chanCount == 1)
                    {
                        b = new StringBuilder("Users in {0}: ".F(chan.Title ?? chan.Name));
                        foreach (var i in chan.Users)
                            b.Append("[user]{0}[/user]".F(i));
                        currentWindow.AddLine(b.ToString(), null);
                    }
                    else
                        currentWindow.AddWarningLine(chanCount > 1
                                                         ? "There are more channels with this title, either go to the tab and write /who again (without the name) or specify the ADH channel name instead"
                                                         : "Cannot find the channel you specified, or you are not in that channel");
                }
                else
                    currentWindow.AddWarningLine("You must be in a channel to see who is in it.");
            }
        }

        private static void SetStatus(string p1, string p2)
        {
            CharacterStatus status;
            Enum.TryParse(p1, true, out status);
            // If it fails it will default to setting it to Online.. which is better than doing nothing at all
            ConnectionManager.GetLastActiveConnection().ChangeStatus(status, p2);
        }

        private static void Login(object p1, object p2)
        {
            var loginShell = IoC.Get<LoginShellViewModel>();
            var window = IoC.Get<ISapphireWindowManager>().CreatePopup(loginShell, new SapphireWindowManager.WindowOptions
                                                                                 {
                                                                                     Title = "Login",
                                                                                     Width = 570,
                                                                                     Height = 420,
                                                                                 });
            (loginShell.ActiveItem as LoginViewModel).UserName = p1 as string;
            (loginShell.ActiveItem as LoginViewModel).SetPassword(p2 as string);
            (loginShell.ActiveItem as LoginViewModel).Login();
        }

        private static void Logout()
        {
            if (ConnectionManager.HealthyConnections().Count == 1)
                ConnectionManager.GetLastActiveConnection().Disconnect();
            else
            {
                IoC.Get<ISapphireWindowManager>().CreatePopup(new CharacterSelectViewModel(ConnectionManager.HealthyConnections().Select(x => x.IdentifiedAs.Identity).ToList(), "",
                x =>
                {
                    if (x == CharacterSelectViewModel.ALLCHARACTERS)
                        foreach (var conn in ConnectionManager.HealthyConnections())
                        conn.Disconnect();
                else
                    ConnectionManager.GetConnection(x).Disconnect();

                }, addAllLoggedIn: true), new SapphireWindowManager.WindowOptions { Title = "Select character to Logout", Width = 570, Height = 420 });
            }
        }

        private static void JoinChannel(string channelName)
        {
            if (ConnectionManager.HealthyConnections().Count == 1)
                ChannelManager.JoinChannel(channelName, ConnectionManager.HealthyConnections()[0].ConnectionId);
            else
            {
                IoC.Get<ISapphireWindowManager>().CreatePopup(new CharacterSelectViewModel(ConnectionManager.HealthyConnections().Select(x => x.IdentifiedAs.Identity).ToList(), "",
                    x =>
                    {
                        if (x == CharacterSelectViewModel.ALLCHARACTERS)
                            foreach (var conn in ConnectionManager.HealthyConnections())
                                ChannelManager.JoinChannel(channelName, conn.ConnectionId);
                        else
                            ChannelManager.JoinChannel(channelName, ConnectionManager.GetConnection(x).ConnectionId);

                    }, addAllLoggedIn: true), new SapphireWindowManager.WindowOptions { Title = "Join channel {0}".F(channelName), Width = 570, Height = 420 });
            }
        }

        private static void SendPrivateMessage(string character, string message)
        {
            var serverCommands = ConnectionManager.GetLastActiveConnection();
            serverCommands.SendPrivateMessage(character, message);
            Events.Publish(new EventMessages.SendPrivateMessage(UserManager.GetUser(character), message, serverCommands.ConnectionId));
        }
    }

    public class ChatCommand
    {
        public int ParamCount { get; private set; }
        public Action<List<string>> Action { get; private set; }

        public ChatCommand(int paramCount, Action<List<string>> action)
        {
            ParamCount = paramCount;
            Action = action;
        }
    }
}