﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using Caliburn.Micro;
using Ninject;
using SapphireChat.Infrastructure.FChat;
using SapphireChat.Infrastructure.LoadModules;
using SapphireChat.Infrastructure.Log;
using SapphireChat.Properties;
using SapphireChat.Infrastructure.Controls;

namespace SapphireChat
{
    internal class AppBootstrapper : Bootstrapper<ViewModels.ShellViewModel>
    {
        private IKernel kernel = new StandardKernel(new MainModule(), new FChatModule());

        protected override void Configure()
        {
            base.Configure();
            NLogConfig();
        }

        private void NLogConfig()
        {
            var config = new NLog.Config.LoggingConfiguration();

            NLog.Targets.FileTarget fileTarget = new NLog.Targets.FileTarget();
            config.AddTarget("file", fileTarget);

            fileTarget.FileName = "${basedir}/logs/${shortdate}.log";
            fileTarget.Layout = "[${longdate}] [${uppercase:${level}}] [${logger}]: ${message}";

            NLog.Config.LoggingRule rule2 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Debug, fileTarget);
            config.LoggingRules.Add(rule2);

            NLog.LogManager.Configuration = config;
        }

        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {
            TextBlock t = new TextBlock();
            t.Inlines.Add(BBCodeParser.Parse("T [b]"));
            var text = t.Text;
            Settings.Default.UseDebug = e.Args.Contains("DEBUG");
#if DEBUG
            Settings.Default.UseDebug = true;
#endif
            if (Settings.Default.UseDebug)
                DebugLog.LogMessage("App started");

            Settings.Default.UseTestServer = e.Args.Contains("TESTSERVER");
            base.OnStartup(sender, e);
        }

        protected override void BuildUp(object instance)
        {
            kernel.Inject(instance);
        }

        protected override object GetInstance(Type service, string key)
        {
            return kernel.Get(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return kernel.GetAll(service);
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            ConnectionManager.KillAll();
            base.OnExit(sender, e);
        }

        protected override void OnUnhandledException(object sender,
                                                     System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            if (Settings.Default.UseDebug)
            {
                DebugLog.LogMessage("Exception! : {0}", e.Exception.ToString());
                e.Handled = true;
            }
        }
    }
}